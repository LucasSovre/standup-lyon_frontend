/** @format */

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./reducers/store";
import Routes from "./Routes";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/styles/font.scss";
import "./assets/styles/variables.scss";
import "./assets/styles/loader.css";
import { Helmet, HelmetProvider } from "react-helmet-async";

const rootElement = document.getElementById("root");

if (rootElement.hasChildNodes()) {
  ReactDOM.hydrate(
    <Provider store={store}>
      <HelmetProvider>
        <Helmet>
          <meta charSet="utf-8" />
          <meta name="author" content="Sovre Lucas" />
          <title>StandUp Lyon</title>
          <meta
            name="decription"
            content="Trouvez un spectacle de stand Up comedie sur Lyon."
          />
        </Helmet>
        <Routes />
      </HelmetProvider>
    </Provider>,
    rootElement
  );
} else {
  ReactDOM.render(
    <Provider store={store}>
      <HelmetProvider>
        <Helmet>
          <meta charSet="utf-8" />
          <meta name="author" content="Sovre Lucas" />
          <title>StandUp Lyon</title>
          <meta
            name="decription"
            content="Trouvez un spectacle de stand Up comedie sur Lyon."
          />
        </Helmet>
        <Routes />
      </HelmetProvider>
    </Provider>,
    rootElement
  );
}
