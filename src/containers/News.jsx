/** @format */

import React, { useEffect, useState, useCallback } from "react";
import BigEvent from "../components/BigEvent";
import axios from "axios";
import { base_url } from "../utils/backend";
import "../assets/styles/News.scss";
import { findNearestDate } from "../utils/eventInterpreter";
// eslint-disable-next-line

export default function News({
  number,
  priceFilter,
  setPriceFilter,
  setMaxPrice,
}) {
  const nbr = number;
  let i = 0;
  const [eventList, seteventList] = useState([]);
  const [displayEvent, setDisplayEvent] = useState([]);

  const fetcheventList = useCallback(async () => {
    const options = { method: "GET" };
    axios
      .get(base_url + "/event/", options)
      .then(function (response) {
        const tmp = [];
        response.data.forEach((event) => {
          const dates = [];
          if (event.dates_list !== null) {
            // eslint-disable-next-line
            for (const [key, value] of Object.entries(event.dates_list)) {
              dates.push(new Date(value));
            }
          }
          const nearest = findNearestDate(new Date(), dates);
          if (nearest !== null) {
            tmp.push(event);
          }
        });
        seteventList(tmp);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);

  useEffect(() => {
    fetcheventList();
  }, [fetcheventList]);

  useEffect(() => {
    const tmp = [];
    eventList.forEach((element) => {
      if (element.price <= priceFilter) {
        tmp.push(element);
      }
    });
    setDisplayEvent(tmp);
  }, [priceFilter, eventList]);

  useEffect(() => {
    let max = 0;
    if (eventList !== undefined) {
      eventList.forEach((element) => {
        if (element.price > max) {
          max = element.price;
        }
      });
      setMaxPrice(max);
      setPriceFilter(max);
    }
  }, [eventList, setMaxPrice, setPriceFilter]);

  return (
    <div>
      <div>
        <div className="BE-eventListContainer">
          {eventList &&
            displayEvent.map((event) => {
              if (i < nbr) {
                i = i + 1;
                return <BigEvent event={event} key={i} />;
              } else {
                return null;
              }
            })}
        </div>
      </div>
    </div>
  );
}
