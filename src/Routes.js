/** @format */

import React, { Suspense, lazy } from "react";
import { Route as Road, Switch, BrowserRouter } from "react-router-dom";
import PrivateRoute from "./utils/reactRouter/PrivateRoute";
import NoMatch from "./pages/public/Nomatch";
import { TrackLocation } from "./utils/reactRouter/TrackLocation";

const MainUnConnect = lazy(() => import("./pages/public/MainUnconnect"));
const Evenements = lazy(() => import("./pages/public/Evenements"));
const CreateAccount = lazy(() => import("./pages/public/CreateAccount"));
const Login = lazy(() => import("./pages/public/Login"));
const Dashboard = lazy(() => import("./pages/private/Dashboard"));
const CreateEvent = lazy(() => import("./pages/private/NewCreateEvent"));
const AddDate = lazy(() => import("./pages/private/AddDate"));
const EventPage = lazy(() => import("./pages/public/event"));
const ModifyEventPage = lazy(() => import("./pages/private/modifyEventPage"));
const ArtistPage = lazy(() => import("./pages/public/ArtistPage"));
const ForgotPassword = lazy(() => import("./pages/public/ForgotPassword"));
const Security = lazy(() => import("./pages/private/Security"));
const Settings = lazy(() => import("./pages/private/Settings"));
const DeleteAccount = lazy(() => import("./pages/private/DeleteAccount"));
const Hummorist = lazy(() => import("./pages/public/Hummorists"));
const Contact = lazy(() => import("./pages/public/contact.js"));
const WhoAreWe = lazy(() => import("./pages/public/WhoAreWe"));
const MentionsLegales = lazy(() => import("./pages/public/MentionsLegales"));
const FAQ = lazy(() => import("./pages/public/FAQ"));
const CheckEmail = lazy(() => import("./pages/public/CheckEmail"));
const AskVerified = lazy(() => import("./pages/private/AskVerified"));
const NewsletterUnsubscribe = lazy(() =>
  import("./pages/public/NewsletterUnsubscribe")
);
const ResetPassword = lazy(() => import("./pages/public/resetPassword"));
const DeactivateAccount = lazy(() =>
  import("./pages/public/deactivateAccount")
);
const AskForDeactivate = lazy(() => import("./pages/public/askForDeactivate"));

function Routes() {
  return (
    <BrowserRouter>
      <Suspense fallback={<div></div>}>
        <TrackLocation />
        <Switch>
          <Road exact path="/" component={MainUnConnect} />
          <Road exact path="/Evenements" component={Evenements} />
          <Road exact path="/CreateAccount" component={CreateAccount} />
          <Road exact path="/login" component={Login} />
          <Road exact path="/forgotPassword" component={ForgotPassword} />
          <Road exact path="/contact" component={Contact} />
          <Road exact path="/newsletter" component={NewsletterUnsubscribe} />
          <Road exact path="/Qui-sommes-nous" component={WhoAreWe} />
          <Road exact path="/mentionsLegales" component={MentionsLegales} />
          <Road exact path="/FAQ" component={FAQ} />
          <Road exact path="/checkEmail/:emailID" component={CheckEmail} />
          <Road
            exact
            path="/resetPassword/:resetID"
            component={ResetPassword}
          />
          <Road
            exact
            path="/deactivateAccount/:deactivateID"
            component={DeactivateAccount}
          />
          <Road exact path="/askForDeactivate/" component={AskForDeactivate} />
          <Road exact path="/event/:eventID" component={EventPage} />
          <Road exact path="/artist" component={Hummorist} />
          <Road exact path="/artist/:artistID" component={ArtistPage} />
          <PrivateRoute exact path="/logedIn/Dashboard" component={Dashboard} />
          <PrivateRoute exact path="/logedIn/settings" component={Settings} />
          <PrivateRoute
            exact
            path="/logedIn/askVerified"
            component={AskVerified}
          />
          <PrivateRoute exact path="/logedIn/security" component={Security} />
          <PrivateRoute
            exact
            path="/logedIn/DeleteAccount"
            component={DeleteAccount}
          />
          <PrivateRoute
            exact
            path="/logedIn/CreateEvent"
            component={CreateEvent}
          />
          <PrivateRoute
            exact
            path="/logedIn/AddDate/:eventID"
            component={AddDate}
          />
          <PrivateRoute
            exact
            path="/logedIn/ModifyEvent/:eventID"
            component={ModifyEventPage}
          />
          <Road component={NoMatch} />
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
}

export default Routes;
