/** @format */

import Cookies from "universal-cookie";

const base_url = "http://0.0.0.0:8001";

function logout() {
  const cookies = new Cookies();
  cookies.remove("token", { path: "/" });
  window.location.reload(false);
}

export { base_url, logout };
