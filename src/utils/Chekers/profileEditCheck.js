function PhoneCheck (input){
    var result = input.match(/^[0]\d{9}$/gm);
    if (result){return true} else {return false}
}

function PostalCheck (input){
 var result = input.match(/^\d{5}$/gm);
 if (result){return true} else {return false}
}

function RemoveWhiteSpace (input){
  var result = input.split(" ").join("");
  return(result) 
}

export {PhoneCheck, PostalCheck, RemoveWhiteSpace};