/** @format */

import imageCompression from "browser-image-compression";

async function compressImg(image) {
  const options = {
    maxSizeMB: 0.3, // (default: Number.POSITIVE_INFINITY)
    maxWidthOrHeight: 1000, // compressedFile will scale down by ratio to a point that width or height is smaller than maxWidthOrHeight (default: undefined)
    // but, automatically reduce the size to smaller than the maximum Canvas size supported by each browser.
    // Please check the Caveat part for details.
    useWebWorker: true, // optional, use multi-thread web worker, fallback to run in main-thread (default: true)

    // following options are for advanced users
    maxIteration: 10, // optional, max number of iteration to compress the image (default: 10)
  };
  return imageCompression(image, options);
}

export { compressImg };
