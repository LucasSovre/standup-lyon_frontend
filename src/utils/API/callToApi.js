import { base_url } from "../backend";

async function fetchMyEvents(username) {
  const query = await fetch(base_url + "/eventByCreator/?creator=" + username, {
    headers: {
      Accept: "application/json, text/plain, */*",
    },
    method: "GET",
  });
  const response = await query.json();
  return response;
}

async function fetchUniqueEvent(id) {
  const query = await fetch(base_url + "/uniqueEvent/?id=" + id, {
    headers: {
      Accept: "application/json, text/plain, */*",
    },
    method: "GET",
  });
  const response = await query.json();
  return response;
}

async function fetchEventCover(file) {
  const query = await fetch(base_url + "/cover/?file=" + file, {
    headers: {
      Accept: "application/json, text/plain, */*",
    },
    method: "GET",
  });
  return query;
}

async function deleteEvent(id, username, token) {
  const query = await fetch(
    base_url + "/event/?id=" + id + "&creator=" + username,
    {
      headers: {
        Accept: "application/json, text/plain, */*",
        Authorization: "Bearer " + token,
      },
      method: "DELETE",
    }
  );
  return query;
}

async function fetchAllArtists() {
  const query = await fetch(base_url + "/usersPublicInfGet/", {
    headers: {
      Accept: "application/json, text/plain, */*",
    },
    method: "GET",
  });
  const response = await query.json();
  return response;
}

export {
  fetchMyEvents,
  fetchEventCover,
  deleteEvent,
  fetchUniqueEvent,
  fetchAllArtists,
};
