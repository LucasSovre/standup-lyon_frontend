/** @format */

import { GiReceiveMoney } from "react-icons/gi";
import { MdOutlineMoneyOffCsred } from "react-icons/md";

/**
 * this funnction translate date iso format to litteral date
 * return a string
 * @param {string} entry - String date in the format iso format.
 * @returns {string} date string formated (exemple : 21/02 21:15)
 */
function date2String(entry, litteral = false) {
  //check entry
  if (typeof entry !== "object") {
    entry = new Date(Date.parse(entry.substring(0, 16).replaceAll("/", "-")));
  }
  const mm = (entry.getMonth() + 1).toString().padStart(2, "0");
  const dd = entry.getDate().toString().padStart(2, "0");
  if (litteral === false) {
    return (
      mm +
      "/" +
      dd +
      " " +
      entry.toLocaleTimeString("fr-FR", { hour: "numeric", minute: "numeric" })
    );
  } else {
    return (
      entry.toLocaleDateString("fr-FR", { month: "long", day: "numeric" }) +
      " " +
      entry.toLocaleTimeString("fr-FR", { hour: "numeric", minute: "numeric" })
    );
  }
}

/**
 *
 * @param {int} price
 * @param {boolean} optional entry - return only text format .
 * @returns JSX element
 */
function renderPrice(price, pureText = false) {
  switch (price) {
    case -1:
      return pureText ? "Au chapeau" : <GiReceiveMoney />;
    case 0:
      return pureText ? "Gratuit" : <MdOutlineMoneyOffCsred />;
    default:
      return price + "€";
  }
}
/**
 *
 * @param {Date} today
 * @param {[Date]} dateList
 * @returns the nearest date of the given dateList.
 */
function findNearestDate(today, dateList) {
  let max = Number.POSITIVE_INFINITY;
  let result = null;
  dateList.forEach((element) => {
    const calcul = element - today;
    if (calcul < max && calcul >= 0) {
      max = calcul;
      result = element;
    }
  });
  return result;
}

export { date2String, renderPrice, findNearestDate };
