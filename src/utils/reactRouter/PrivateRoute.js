/** @format */

import React from "react";
import { Route, Redirect } from "react-router-dom";
import Cookies from "universal-cookie";
import { useSelector, useDispatch } from "react-redux";
import { setProfile, setPP } from "../../actions/actionIndex";
import defaultAvatar from "../../assets/img/defaultAvatar.png";
import axios from "axios";
import store from "../../reducers/store";
import { base_url } from "../backend";

const fetchPP = async (name) => {
  const options = {
    method: "GET",
    url: base_url + "/photoProfil/",
    headers: { "Content-Type": "application/json" },
    responseType: "blob",
    params: { username: name },
  };
  try {
    const resp = await axios
      .get(base_url + "/photoProfil/", options)
      .then(function (response) {
        return new Blob([response.data], { type: "image/jpeg" });
      });
    store.dispatch(setPP(URL.createObjectURL(resp)));
    return resp;
  } catch (err) {}
};

const PrivateRoute = ({ component: Component, ...rest }) => {
  const cookies = new Cookies();
  const dispatch = useDispatch();
  const token = cookies.get("token");
  const profileStatus = useSelector((state) => state.profil.status);
  const profile = useSelector((state) => state.profil.user);
  const pp = useSelector((state) => state.currentPP.pp);
  if (profileStatus === null || profileStatus === "failed") {
    dispatch(setProfile());
  }
  if (pp === defaultAvatar && profile) {
    setTimeout(() => {
      fetchPP(profile.username);
    }, 100);
  }

  return (
    // Show the component only when the user is logged in
    // Otherwise, redirect the user to /signin page
    <Route
      {...rest}
      render={(props) =>
        token !== undefined ? (
          <Component {...props} />
        ) : (
          <Redirect to="/login" />
        )
      }
    />
  );
};

export default PrivateRoute;
