/** @format */

import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { react_umami } from "react-umami";

const tracker = new react_umami(
  "3f08b913-7a87-483e-a297-e4ad1124bee5",
  "https://standup-lyon.fr/",
  "https://analytics.standup-lyon.fr/umami.js"
);

function TrackLocation() {
  const location = useLocation();

  useEffect(() => {
    tracker.trackView(location.pathname);
  }, [location]);

  return null;
}

export { TrackLocation, tracker };
