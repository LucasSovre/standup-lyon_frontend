/** @format */

function formatSearch(entry, setSearchPlace) {
  // eslint-disable-next-line
  const middle = entry
    .toLowerCase()
    .replace(/[.,/#!$%^&*;:{}=\-_`~()]/g, "")
    .split(" ");
  let finalquerry = "https://api-adresse.data.gouv.fr/search/?q=";
  for (const item in middle) {
    if (middle[item] !== "") {
      finalquerry = finalquerry + "+" + middle[item];
    }
  }
  setSearchPlace(finalquerry);
} //it format the adress search to the gouvernment api

function RenderSnipet({ searchPlaceResult, setSearchPlaceResult, setAdresse }) {
  if (searchPlaceResult !== []) {
    return (
      <ul className="Form-snipetList">
        {searchPlaceResult && searchPlaceResult.length
          ? searchPlaceResult.map((item) => {
              return (
                <li
                  key={item.properties.id}
                  className=""
                  onClick={() => {
                    document.getElementById("adress-input").value =
                      item.properties.label;
                    setSearchPlaceResult([]);
                    setAdresse(item.properties.label);
                  }}>
                  {item.properties.label}
                </li>
              );
            })
          : null}
      </ul>
    );
  } else {
    return <div></div>;
  }
}

export { RenderSnipet, formatSearch };
