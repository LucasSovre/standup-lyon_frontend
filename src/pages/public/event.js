/** @format */

import React, { useState, useCallback, useEffect } from "react";
import { useParams } from "react-router-dom";
import "../../assets/styles/EventPage.scss";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import axios from "axios";
import { base_url } from "../../utils/backend";
import ArtistIcon from "../../components/artistIcon";
import { renderPrice } from "../../utils/eventInterpreter";

export default function EventPage() {
  let params = useParams();

  const [event, setevent] = useState({});
  const [avaibleDates, setAvaibleDates] = useState([]);
  const [poster, setPoster] = useState(null);

  const fetchCover = useCallback(async () => {
    const options = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
      responseType: "blob",
      params: { file: event.cover },
    };
    axios
      .get(base_url + "/cover/", options)
      .then(function (response) {
        setPoster(URL.createObjectURL(new Blob([response.data])));
      })
      .catch(function (error) {});
  }, [event.cover]);

  useEffect(() => {
    if (event.dates_list !== undefined) {
      const tmp = [];
      Object.values(event.dates_list).forEach((element) => {
        if (new Date() - new Date(element) <= 0) {
          tmp.push(new Date(element));
        }
      });
      console.log(tmp.sort());
      setAvaibleDates(tmp);
    }
  }, [event.dates_list, event.endDate]);

  useEffect(() => {
    fetchCover();
    console.log(event.artists);
  }, [fetchCover, event]);

  const fetchevent = useCallback(async () => {
    const options = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
      params: { id: params.eventID },
    };
    axios
      .request(base_url + "/uniqueEvent/", options)
      .then(function (response) {
        setevent(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, [params.eventID]);

  useEffect(() => {
    fetchevent();
  }, [fetchevent]);

  return (
    <div>
      <Menu />
      <div id="EV-main">
        <h1>{event.name}</h1>
        <div
          id="EV-cover"
          style={{
            backgroundImage: `url(${poster})`,
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}></div>
        <div id="EV-date-container">
          {avaibleDates.map((item) => {
            return (
              <div key={item} className="EV-date-item">
                <h5>{item.toLocaleString("fr-FR", { day: "numeric" })}</h5>
                <h6>
                  {item
                    .toLocaleString("fr-FR", { month: "short" })
                    .replace(".", "")}
                </h6>
                <span></span>
                <h6>
                  {item
                    .toLocaleString("fr-FR", { weekday: "short" })
                    .replace(".", "")}
                </h6>
              </div>
            );
          })}
        </div>
        <div className="EV-detail">
          <h4>Description :</h4>
          <p>{event.description}</p>
        </div>
        <div className="EV-detail">
          <h4>Prix :</h4>
          <p>{renderPrice(event.price, true)}</p>
        </div>
        <div className="EV-detail">
          <h4>Adresse :</h4>
          <p>{event.adresse}</p>
        </div>
        {event.artists ? (
          Object.entries(event.artists).length !== 0 ? (
            <div className="EV-detail">
              <h4>Les humoristes :</h4>
              <div id="EV-caroussel">
                {event.artists &&
                  Object.entries(event.artists).map((artists) => {
                    return (
                      <ArtistIcon key={artists} artist={artists.slice(1)} />
                    );
                  })}
              </div>
            </div>
          ) : null
        ) : null}
      </div>
      <div id="EV-bottomBtn">
        {event.bookLink !== "None" &&
        event.bookLink !== "" &&
        event.bookLink !== null ? (
          <a href={event.bookLink} id="EV-book">
            RÉSERVER
          </a>
        ) : null}
      </div>
      <Footer />
    </div>
  );
}
