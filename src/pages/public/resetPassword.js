/** @format */

import React, { useEffect, useState } from "react";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import { base_url } from "../../utils/backend";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";

import "../../assets/styles/Login.scss";
import "../../assets/styles/resetPassword.scss";

export default function ResetPassword() {
  let params = useParams();
  const history = useHistory();

  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [mdpWarning, setMdpWarning] = useState("");

  useEffect(() => {
    const len = password1.length;
    if (password1.match(/\w/g) && len < 8) {
      document.getElementById("password1-check").style.visibility = "visible";
      document.getElementById("password1-green").style.width = len * 10 + "%";
      document.getElementById("password1-red").style.width =
        (7 - len) * 10 + 30 + "%";
      document.getElementById("password1-green").style.background = "red";
      setMdpWarning("Le mot de passe dois contenir au moins 8 charactères.");
    }
    if (len === 8) {
      document.getElementById("password1-check").style.visibility = "visible";
      document.getElementById("password1-green").style.width = "80%";
      document.getElementById("password1-red").style.width = "20%";
      document.getElementById("password1-green").style.background = "orange";
      setMdpWarning("Mot de passe valide mais peu robuste.");
    }
    if (len > 8) {
      document.getElementById("password1-check").style.visibility = "visible";
      document.getElementById("password1-green").style.width = "100%";
      document.getElementById("password1-red").style.width = "00%";
      document.getElementById("password1-green").style.background =
        "greenyellow";
      setMdpWarning("");
    }
    if (!password1.match(/\w/g)) {
      document.getElementById("password1-check").style.visibility = "hidden";
      document.getElementById("password1-green").style.width = "0%";
      document.getElementById("password1-red").style.width = "100%";
      setMdpWarning("");
    }
  }, [password1]);

  useEffect(() => {
    const len = password2.length;
    if (password2.match(/\w/g) && len < 8) {
      document.getElementById("password2-check").style.visibility = "visible";
      document.getElementById("password2-green").style.width = len * 10 + "%";
      document.getElementById("password2-red").style.width =
        (7 - len) * 10 + 30 + "%";
      document.getElementById("password2-green").style.background = "red";
      setMdpWarning("Le mot de passe dois contenir au moins 8 charactères.");
    }
    if (len === 8) {
      if (password1 === password2) {
        document.getElementById("password2-check").style.visibility = "visible";
        document.getElementById("password2-green").style.width = "80%";
        document.getElementById("password2-red").style.width = "20%";
        document.getElementById("password2-green").style.background = "orange";
        setMdpWarning("Mot de passe valide mais peu robuste.");
      } else {
        document.getElementById("password2-check").style.visibility = "visible";
        document.getElementById("password2-green").style.width = len * 10 + "%";
        document.getElementById("password2-red").style.width =
          (7 - len) * 10 + 30 + "%";
        document.getElementById("password2-green").style.background = "red";
        setMdpWarning("Les deux mots de passes ne corréspondent pas.");
      }
    }
    if (len > 8) {
      if (password1 === password2) {
        document.getElementById("password2-check").style.visibility = "visible";
        document.getElementById("password2-green").style.width = "100%";
        document.getElementById("password2-red").style.width = "00%";
        document.getElementById("password2-green").style.background =
          "greenyellow";
        setMdpWarning("");
      } else {
        document.getElementById("password2-check").style.visibility = "visible";
        document.getElementById("password2-green").style.width = len * 10 + "%";
        document.getElementById("password2-red").style.width =
          (7 - len) * 10 + 30 + "%";
        document.getElementById("password2-green").style.background = "red";
        setMdpWarning("Les deux mots de passes ne corréspondent pas.");
      }
    }
    if (!password2.match(/\w/g)) {
      document.getElementById("password2-check").style.visibility = "hidden";
      document.getElementById("password2-green").style.width = "0%";
      document.getElementById("password2-red").style.width = "100%";
      setMdpWarning("");
    }
  }, [password1, password2]);

  async function reset() {
    const settings = {
      password: password1,
      arg: "reset",
      key: params.resetID,
    };
    fetch(base_url + "/resetPassword/", {
      body: new URLSearchParams(settings),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "POST",
    }).then((response) => {
      if (response.status === 200) {
        alert("Votre mot de passe à été réinitialisé.");
        setTimeout(() => {
          history.push("/");
        }, 2000);
      } else {
        alert("an error ocured, your password has not been changed.");
        setTimeout(() => {
          history.push("/");
        }, 4000);
      }
    });
  }

  return (
    <div>
      <Menu />
      <div>
        <h3 id="NST-U-title">Réinitialiser votre mot de passe :</h3>
        <div className="RPA-input-container">
          <div className="L-formItem">
            <label>Mot de passe</label>
            <input
              autocomplete={false}
              type="password"
              onChange={(e) => {
                setPassword1(e.target.value);
              }}
            />
            <div id="password1-check" className="L-checkingcontnair">
              <div id="password1-green" className="L-checkGreen"></div>
              <div id="password1-red" className="L-checkRed"></div>
            </div>
            <p>{mdpWarning}</p>
            <label>Confirmez votre mot de passe</label>
            <input
              autocomplete={false}
              type="password"
              onChange={(e) => {
                setPassword2(e.target.value);
              }}
            />
            <div id="password2-check" className="L-checkingcontnair">
              <div id="password2-green" className="L-checkGreen"></div>
              <div id="password2-red" className="L-checkRed"></div>
            </div>
            <p>{mdpWarning}</p>
          </div>
          <button
            id="RPA-submit-btn"
            onClick={() => {
              reset();
            }}>
            Réinitialiser
          </button>
        </div>
      </div>
      <Footer />
    </div>
  );
}
