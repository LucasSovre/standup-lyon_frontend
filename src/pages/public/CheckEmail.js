/** @format */

import React, { useEffect, useState, useCallback } from "react";
import { Link, useParams } from "react-router-dom";
import spiner from "../../assets/img/spinner.svg";
import { base_url } from "../../utils/backend";
import { BiErrorAlt } from "react-icons/bi";
import { BsCheckCircle } from "react-icons/bs";

import Menu from "../../components/Menu";

import "../../assets/styles/checkEmail.css";

export default function CheckEmail() {
  let params = useParams();
  const [result, setResult] = useState("");
  const [status, setStatus] = useState(0);

  function renderResult() {
    if (status === 0) {
      return <img src={spiner} alt="loading icon" />;
    }
    if (status === 200) {
      return (
        <div className="CE-main">
          <h2>Votre email est désormais verifié.</h2>
          <div id="CE-icon-valid">
            <BsCheckCircle />
          </div>
        </div>
      );
    } else {
      return (
        <div className="CE-main">
          <h2>
            Une erreur a eu lieu, merci de{" "}
            <Link to="/contact">nous contacter</Link> avec le code erreur
            ci-dessous :
          </h2>
          <div id="CE-error">
            <div id="CE-icon-error">
              <BiErrorAlt />
            </div>
            <p>Code erreur : {result}</p>
          </div>
        </div>
      );
    }
  }

  const fecthmail = useCallback(async () => {
    await fetch(base_url + "/emailCheck/", {
      method: "POST",
      body: new URLSearchParams({
        key: params.emailID,
      }),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    }).then((response) => {
      if (response.status === 200) {
        response.json().then((data) => {
          setStatus(200);
          setResult("");
        });
      } else {
        response.json().then((data) => {
          setStatus(400);
          setResult(data.error);
        });
      }
    });
  }, [params.emailID]);

  useEffect(() => {
    fecthmail();
  }, [fecthmail]);

  return (
    <div>
      <Menu />
      {renderResult()}
    </div>
  );
}
