/** @format */

import { Suspense, lazy, useState } from "react";
import "../../assets/styles/MainUnConnect.scss";
import { Helmet } from "react-helmet-async";
import logo from "../../assets/img/comedian.webp";
import screenEvent from "../../assets/img/screenShotEvent.png";
import pp1 from "../../assets/img/landing/malik.webp";
import pp2 from "../../assets/img/fake/pp2.jpeg";
import pp3 from "../../assets/img/fake/pp3.jpeg";
const News = lazy(() => import("../../containers/News"));
const Menu = lazy(() => import("../../components/Menu"));
const Footer = lazy(() => import("../../components/Footer"));

export default function MainUnConnect() {
  const [priceFilter, setPriceFilter] = useState(100);
  // eslint-disable-next-line
  const [maxPrice, setMaxPrice] = useState(100);

  return (
    <div>
      <Helmet>
        <title>StandUp Lyon ce soir</title>
        <meta
          name="decription"
          content="Trouvez un spectacle de stand Up comedie sur Lyon."
        />
      </Helmet>
      <Suspense fallback={<div></div>}>
        <Menu />
        <div id="MU-main">
          <img id="MU-logo" src={logo} alt="website logo" />
          <h1>
            Bienvenue sur <br /> <span>Standup-lyon !</span>
          </h1>
          <div id="MU-artists">
            <h2>
              Découvrez des <span>Artistes</span> de <span>Lyon</span> et
              d'ailleurs
            </h2>
            <div id="MU-artists-img">
              <img src={pp1} alt="prez1" />
              <img src={pp2} alt="prez2" />
              <img src={pp3} alt="prez3" />
            </div>
          </div>
          <h2 id="MU-book">
            <span>Réservez</span> votre place pour un <span>évènement</span>
          </h2>
          <div id="MU-next-event">
            <h3>Prochain évènements :</h3>
            <News
              number={4}
              priceFilter={priceFilter}
              setPriceFilter={setPriceFilter}
              setMaxPrice={setMaxPrice}
            />
          </div>
          <div id="MU-event">
            <h2>
              <span>Proposez</span> vous-même un <span>spectacle </span>!
            </h2>
            <img src={screenEvent} alt="event screen shot" />
          </div>
        </div>
        <Footer />
      </Suspense>
    </div>
  );
}
