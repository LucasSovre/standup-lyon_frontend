/** @format */

import img from "../../assets/img/404img.png";

export default function NoMatch() {
	return (
		<div className="container d-flex flex-column text-center">
			<h2>Error 404, sorry</h2>
			<p>We can't find what you are looking for please check the url</p>
			<img className="img-fluid" src={img} alt="404 page not found" />
		</div>
	);
}
