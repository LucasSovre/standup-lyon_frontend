/** @format */

import React from "react";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer.jsx";
import loanimg from "../../assets/img/loan.jpg";
import ninaImg from "../../assets/img/nina.png";
import lucasImg from "../../assets/img/lucas_img.jpg";

import "../../assets/styles/WhoAreWe.scss";

export default function WhoAreWe() {
  return (
    <div>
      <Menu />
      <h1 id="WO-title">
        Qui sommes <br /> nous ?
      </h1>
      <div id="WO-main">
        <h2>L'équipe</h2>
        <div id="WO-team">
          <div className="WO-team-item">
            <img src={lucasImg} alt="Lucas, the funder and lead dev" />
            <h3>Lucas Sovre</h3>
            <h4>Créateur et développeurdu site.</h4>
          </div>
          <div className="WO-team-item">
            <img src={loanimg} alt="Loann, our designer" />
            <h3>Loann Benoit</h3>
            <h4>UX/UI Designer du site.</h4>
          </div>
          <div className="WO-team-item">
            <img src={ninaImg} alt="Loann, our artist" />
            <h3>Nina Mallick</h3>
            <h4>Illustratrice.</h4>
          </div>
        </div>
        <h2>Le site</h2>
        <div className="WO-text">
          <p>
            Ce site web est à but non-lucratif. Il ne récolte que le strict
            nécessaire en termes de données personnelles.Notre but est simple :
            faire la promotion du stand-up dans Lyon et ses alentours.
          </p>
          <p>
            Le site vit uniquement grâce à la pubet à l'argent généré par le
            stand-up.(la pub sert paye uniquement les frais du serveur web,
            administration, etc.
          </p>
        </div>
        <h2>Nous faire confiance ?</h2>
        <div className="WO-text">
          <p>
            Nous sommes bénévoles, ce site n'ayant pas de but lucratif, vous ne
            risquez rien à venir rireà nos évènements.
          </p>
        </div>
        <p id="WO-warning">
          Ce site web est un hébergement de contenu, seuls les auteurs des
          publications sont responsables de leurs publications. Si un contenu
          vous semble inapproprié ou choquant, merci de le signaler au plus vite
          via la page contact.
        </p>
      </div>
      <Footer />
    </div>
  );
}
