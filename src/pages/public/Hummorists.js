/** @format */

import React, { useCallback, useEffect, useState } from "react";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import LittleArtist from "../../components/littleArist";
import axios from "axios";
import { base_url } from "../../utils/backend";
import { Helmet } from "react-helmet-async";
import { FiFilter } from "react-icons/fi";

import "../../assets/styles/Hummorist.scss";

export default function Hummorist() {
  const [artistList, setArtistList] = useState([]);
  const [suffledArtistList, setShuffledArtistList] = useState([]);
  const [artistListSearch, setartistListSearch] = useState([]);

  const fetchArtistList = useCallback(async () => {
    const options = {
      method: "GET",
    };
    axios
      .get(base_url + "/usersPublicInfGet/", options)
      .then(function (response) {
        setArtistList(response.data);
        setartistListSearch(response.data);
      })
      .catch(function (error) {});
  }, []);

  useEffect(() => {
    fetchArtistList();
  }, [fetchArtistList]);

  useEffect(() => {
    setShuffledArtistList(artistList.sort((a, b) => 0.5 - Math.random()));
  }, [artistList]);

  function artistListSearchFunction(entry) {
    let temporaryArray = [];
    for (const i in suffledArtistList) {
      if (
        suffledArtistList[i].username
          .toLowerCase()
          .includes(entry.toLowerCase())
      ) {
        temporaryArray.push(suffledArtistList[i]);
      }
    }
    setartistListSearch([...temporaryArray]);
  }

  return (
    <div>
      <Helmet>
        <title>Hummoristes lyonnais, standup comedie</title>
        <meta
          name="decription"
          content="Retrouvez tout vos hummoristes lyonnais sur Standup Comedie lyon"
        />
      </Helmet>
      <Menu />
      <div className="HU">
        <h2>
          Les <br />
          Humoristes
        </h2>
        <div className="HU-input-container">
          <FiFilter />
          <input
            className="HU-search"
            type="text"
            onChange={(e) => {
              artistListSearchFunction(e.target.value);
            }}
          />
        </div>
        <div className="HU-artistCaroussel">
          {artistListSearch.map((artist) => {
            return <LittleArtist artist={artist} key={artist.username} />;
          })}
        </div>
      </div>
      <Footer />
    </div>
  );
}
