/** @format */

import React, { useState } from "react";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";

import { base_url } from "../../utils/backend";
import { useHistory } from "react-router-dom";

import "../../assets/styles/form.scss";

export default function ForgotPassword() {
  const [email, setEmail] = useState("");
  const history = useHistory();

  function AskresetPassword() {
    const settings = {
      email: email,
      arg: "ask",
    };
    fetch(base_url + "/resetPassword/", {
      body: new URLSearchParams(settings),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "POST",
    }).then((response) => {
      alert("Si l'email existe un email lui à été envoyé.");
      setTimeout(() => {
        history.push("/");
      }, 2000);
    });
  }

  return (
    <div>
      <Menu />
      <div className="F-container">
        <div className="L-formItem">
          <label>Email :</label>
          <input
            type="email"
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
        </div>
        <button
          type="submit"
          id="F-submit-btn"
          onClick={() => {
            AskresetPassword();
          }}>
          Réinitialiser mot de passe
        </button>
      </div>
      <Footer />
    </div>
  );
}
