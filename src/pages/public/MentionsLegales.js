/** @format */

import React from "react";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import cgu from "../../assets/légal/cgu.pdf";
import mentions from "../../assets/légal/MENTIONS_LEGALES.pdf";
import RGPD from "../../assets/légal/RGPD.pdf";

import "../../assets/styles/MentionLegales.scss";

export default function MentionLegales() {
  return (
    <div>
      <Menu />
      <div className="ML-container">
        <h1>Mentions légales </h1>
        <div className="ML-dl-container">
          <p>Toutes les CGU :</p>
          <a
            style={{ textDecoration: "none" }}
            className="ML-dl"
            href={cgu}
            download="cgu.pdf">
            TÉLÉCHARGER
          </a>
          <p>Mentions légales :</p>
          <a
            style={{ textDecoration: "none" }}
            className="ML-dl"
            href={mentions}
            download="mentions légales.pdf">
            TÉLÉCHARGER
          </a>
          <p>RGPD :</p>
          <a
            style={{ textDecoration: "none" }}
            className="ML-dl"
            href={RGPD}
            download="RGPD.pdf">
            TÉLÉCHARGER
          </a>
        </div>
      </div>
      <Footer />
    </div>
  );
}
