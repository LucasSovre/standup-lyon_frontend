/** @format */

import React from "react";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import { useParams, useHistory } from "react-router-dom";
import { base_url } from "../../utils/backend";

import { FaUnlockAlt } from "react-icons/fa";
import { FaLock } from "react-icons/fa";

import "../../assets/styles/deactivateAccount.css";

export default function DeactivateAccount() {
  const params = useParams();
  const history = useHistory();

  function deactivateAccount() {
    const settings = {
      arg: "reset",
      key: params.deactivateID,
    };
    fetch(base_url + "/deactivateAccount/", {
      body: new URLSearchParams(settings),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "POST",
    }).then((response) => {
      if (response.status === 200) {
        document.getElementById("DEA-unlock").hidden = true;
        document.getElementById("DEA-lock").hidden = false;
        alert("Votre compte est désormais : désactivé.");
        setTimeout(() => {
          history.push("/");
        }, 2000);
      }
    });
  }

  return (
    <div>
      <Menu />
      <div className="DEA-container">
        <h3>Desactiver votre compte :</h3>
        <div className="DEA-warning">
          <p>
            Attention ! si vous désactivez votre compte, plus personne ne
            pourras se connecter à votre compte. Pour recuperer vos accés vous
            devrez contacter nos admins.
          </p>
          <div id="DEA-unlock">
            <FaUnlockAlt />
          </div>
          <div id="DEA-lock" hidden={true}>
            <FaLock />
          </div>
        </div>
        <button
          onClick={() => {
            deactivateAccount();
          }}>
          Désactiver
        </button>
      </div>
      <Footer />
    </div>
  );
}
