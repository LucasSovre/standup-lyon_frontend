/** @format */

import React, { useCallback, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import BigEvent from "../../components/BigEvent";
import axios from "axios";
import { base_url } from "../../utils/backend";
import defaultAvatar from "../../assets/img/defaultAvatar.png";
import { Helmet } from "react-helmet-async";

import { FaYoutube } from "react-icons/fa";
import { AiFillInstagram, AiOutlineTwitter } from "react-icons/ai";

import "../../assets/styles/ArtistPage.scss";

function ArtistPage() {
  const params = useParams();
  const [artist, setArtist] = useState({});
  const [eventList, seteventList] = useState([]);
  const [PP, setPP] = useState(defaultAvatar);

  const fetchPP = useCallback(async () => {
    if (artist.username) {
      const options = {
        method: "GET",
        url: base_url + "/photoProfil/",
        headers: { "Content-Type": "application/json" },
        responseType: "blob",
        params: { username: artist.username },
      };
      try {
        const resp = await axios
          .get(base_url + "/photoProfil/", options)
          .then(function (response) {
            return new Blob([response.data], {
              type: "image/jpeg",
            });
          });
        setPP(URL.createObjectURL(resp));
        return resp;
      } catch (err) {}
    }
  }, [artist.username]);

  const fetchArtists = useCallback(async () => {
    const form = new FormData();
    form.append("username", params.artistID);
    const options = {
      method: "POST",
      headers: {
        "Content-Type":
          "multipart/form-data; boundary=---011000010111000001101001",
      },
      data: form,
    };
    axios
      .request(base_url + "/usersPublicInfGet/", options)
      .then(function (response) {
        setArtist(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, [params.artistID]);

  const fetcheventList = useCallback(async () => {
    const options = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
      params: { artist: params.artistID },
    };
    axios
      .get(base_url + "/eventByArtist/", options)
      .then(function (response) {
        seteventList(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, [params.artistID]);

  useEffect(() => {
    fetchPP();
    fetchArtists();
    fetcheventList();
  }, [fetchArtists, fetcheventList, fetchPP]);

  return (
    <div>
      <Helmet>
        <title>{params.artistID + " StandUp"}</title>
      </Helmet>
      <Menu />
      <div id="AP-main">
        <h1>{artist.username}</h1>
        <div id="AP-profile">
          <img src={PP} alt="avatar de l'artiste" />
          <div id="AP-content">
            <div id="AP-description">
              <h1>Présentation :</h1>
              <p>
                {artist.publicData
                  ? artist.publicData.description
                    ? artist.publicData.description
                    : null
                  : null}
              </p>
            </div>
            <div id="AP-social">
              <h1>Réseaux sociaux :</h1>
              <div>
                {artist.publicData ? (
                  artist.publicData.youtubeAcount ? (
                    <a href={artist.publicData.youtubeAcount}>
                      <FaYoutube />
                    </a>
                  ) : null
                ) : null}
                {artist.publicData ? (
                  artist.publicData.twitterUserName ? (
                    <a
                      href={
                        "https://twitter.com/" +
                        artist.publicData.twitterUserName.slice(1)
                      }>
                      <AiOutlineTwitter />
                    </a>
                  ) : null
                ) : null}
                {artist.publicData ? (
                  artist.publicData.instagramUserName ? (
                    <a
                      href={
                        "https://www.instagram.com/" +
                        artist.publicData.instagramUserName.slice(1)
                      }>
                      <AiFillInstagram />
                    </a>
                  ) : null
                ) : null}
              </div>
            </div>
          </div>
        </div>
        <div className="AP-eventListContainer">
          <h2>Prochains évènements :</h2>
          {eventList &&
            eventList.map((event) => {
              return <BigEvent event={event} key={event.name} />;
            })}
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default ArtistPage;
