/** @format */

import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import "../../assets/styles/Login.scss";
import Cookies from "universal-cookie";
import { useDispatch } from "react-redux";
import { setProfile } from "../../actions/actionIndex";
import Menu from "../../components/Menu";
import { base_url } from "../../utils/backend";
import "../../assets/styles/Forms.scss";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // eslint-disable-next-line
  const [warning, setWarning] = useState("");
  const history = useHistory();
  const cookies = new Cookies();
  const token = cookies.get("token");
  const dispatch = useDispatch();

  function signIn() {
    fetch(base_url + "/auth/token", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => {
        if (response.status === 200) {
          response.json().then((data) => {
            cookies.set("token", data.access, {
              path: "/",
              secure: true,
              sameSite: "strict",
              maxAge: 3600,
            });
            dispatch(setProfile());
          });
        } else {
          if (response.status === 400) {
            response.json().then((data) => {
              if (data.error === "invalid_request") {
                setWarning(
                  "L' email ou le mot de passe est manquant, merci de remplir tout les champs."
                );
              }
              if (data.error === "invalid_grant") {
                setWarning("L' email ou le mot de passe est invalide.");
              }
            });
          }
        }
      })
      .then(() => {
        if (warning === "") {
          setTimeout(() => {
            history.push("/logedIn/Dashboard");
          }, 1000);
        }
      });
  }

  useEffect(() => {
    if (token !== "") {
      if (token !== undefined) {
        history.push("/logedIn/Dashboard");
      }
    }
  }, [token, history]);

  return (
    <div>
      <Menu />
      <div>
        <h1 id="LO-title">Connexion</h1>

        <form
          className="Forms-main"
          onSubmit={(e) => {
            e.preventDefault();
            signIn();
          }}>
          <div className="Forms-group">
            <label>Email</label>
            <input
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              placeholder="Email"
              type="text"
            />
          </div>
          <div className="Forms-group">
            <label>Mot de passe</label>
            <input
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              placeholder="Mot de passe"
              type="password"
            />
          </div>
          <input type="submit" />
          <Link to="/forgotPassword">Mot de passe oublié ?</Link>
          <div className="Forms-group LO-submit">
            <p>Vous n'avez pas de compte ?</p>
            <Link to="/CreateAccount">Créer un compte</Link>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
