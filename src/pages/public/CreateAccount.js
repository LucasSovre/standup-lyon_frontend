/** @format */

import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import cgu from "../../assets/légal/cgu.pdf";
import RGPD from "../../assets/légal/RGPD.pdf";
import { base_url } from "../../utils/backend";
import Cookies from "universal-cookie";
import { useDispatch } from "react-redux";
import { setProfile } from "../../actions/actionIndex";
import "../../assets/styles/Forms.scss";
import "../../assets/styles/CreateAccount.scss";
import Menu from "../../components/Menu";
import "../../assets/styles/Login.scss";

function CreateAccount() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [surname, setSurname] = useState("");
  const [username, setUsername] = useState("");
  const [name, setName] = useState("");
  const [legal, setlegal] = useState(false);

  const history = useHistory();
  const cookies = new Cookies();
  const dispatch = useDispatch();

  function checkField() {
    if (username.includes(" ")) {
      if (
        window.confirm(
          "Votre champ " +
            username +
            " contient un espace, êtes vous d'accord pour le remplacer par : \n" +
            username.replace(" ", "_")
        )
      ) {
        setUsername(username.replace(" ", "_"));
        document.getElementById("usernameInput").value = username.replace(
          " ",
          "_"
        );
      } else {
        alert(
          "merci de supprimer les espaces ou autre charactères que @ . + - _ du champ + " +
            username
        );
        document.getElementById("usernameInput").style.backgroundColor = "red";
      }
    }
    if (!email.includes("@") && !email.includes(".")) {
      alert("votre email est incorrecte");
    }
  }

  async function CreateUser() {
    checkField();
    if (legal) {
      await fetch(base_url + "/register/", {
        method: "POST",
        body: new URLSearchParams({
          username: username,
          password: password,
          password2: password,
          email: email,
          first_name: surname,
          last_name: name,
        }),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }).then((response) => {
        console.log(Math.floor(response.status / 100));
        if (Math.floor(response.status / 100) === 4) {
          response.json().then((data) => {
            console.log(data);
            if (data["error-message"]) {
              if (data["error-message"].includes("associé")) {
                alert(
                  "Ce pseudo est déja pris. Si vous êtes connu sous ce pseudo, contactez l'administrateur du site."
                );
              }
              if (data.password) {
                if (data.password[0].includes("common")) {
                  alert(
                    "Ce mot de passe est trop commun, merci d'essayer avec un autre mot de passe."
                  );
                }
              } else {
                alert("Veuillez remplir correctement tout les champs.");
              }
            }
          });
        }
      });
      fetch(base_url + "/auth/token", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      })
        .then((response) => {
          if (response.status === 200) {
            response.json().then((data) => {
              cookies.set("token", data.access, {
                path: "/",
                secure: true,
                sameSite: "strict",
                maxAge: 3600,
              });
              dispatch(setProfile());
            });
          } else {
            console.error("a log error occured");
          }
        })
        .then(() => {
          setTimeout(() => {
            if (cookies.get("token") !== undefined) {
              history.push("/logedIn/Dashboard");
            }
          }, 500);
        });
    } else {
      alert("Vous devez accepter nos mentions légales pour creer un compte.");
    }
  }

  return (
    <div>
      <Menu />
      <h1 id="CA-title">Créer un compte</h1>
      <div className="Forms-main form">
        <div className="Forms-group">
          <label>Email</label>
          <input
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            type="text"
            placeholder="Email"
          />
        </div>
        <div className="Forms-group-double">
          <div className="Forms-group">
            <label>Nom</label>
            <input
              onChange={(e) => {
                setName(e.target.value);
              }}
              type="text"
              placeholder="Nom"
            />
          </div>
          <div className="Forms-group">
            <label>Prénom</label>
            <input
              onChange={(e) => {
                setSurname(e.target.value);
              }}
              type="text"
              placeholder="Prénom"
            />
          </div>
        </div>
        <div className="Forms-group">
          <label>Pseudo</label>
          <input
            id="usernameInput"
            onChange={(e) => {
              setUsername(e.target.value);
            }}
            type="text"
            placeholder="Pseudo"
          />
        </div>
        <div className="Forms-group">
          <label>Mot de passe</label>
          <input
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            type="password"
            placeholder="Mot de passe"
          />
        </div>
        <div className="Forms-checkbox">
          <input
            onChange={(e) => {
              setlegal(e.target.value);
            }}
            type="checkbox"
          />
          <p>
            J'accepte <a href={cgu}>les mentions légales</a>,{" "}
            <a href={RGPD}>la politique de gestion des données</a> et les
            cookies.
          </p>
        </div>
        <button
          onClick={() => {
            CreateUser();
          }}
          type="submit">
          Créez un compte
        </button>
      </div>
    </div>
  );
}

export default CreateAccount;
