/** @format */

import React, { useState } from "react";
import Menu from "../../components/Menu";
import "../../assets/styles/evenements.scss";
import { FiFilter } from "react-icons/fi";
import News from "../../containers/News";
import Footer from "../../components/Footer";

export default function Evenements() {
  const [priceFilter, setPriceFilter] = useState(5);
  const [maxPrice, setMaxPrice] = useState(5);

  return (
    <div>
      <Menu />

      <div id="EVE-main">
        <h1>
          Les <br /> évènements
        </h1>
        {/*<button
          id="EVE-toogle-filter"
          onClick={() => {
            document
              .getElementById("EVE-filter")
              .classList.toggle("EVE-filter-active");
          }}>
          <FiFilter />
        </button>
        <div id="EVE-filter">
          <h4>Filtres</h4>
          <div>
            <label>Gratuit</label>
            <input
              id="EVE-free"
              className="EVE-switch"
              type="checkbox"
              onClick={() => {
                setPriceFilter(0);
                if (priceFilter === 0) {
                  setPriceFilter(maxPrice);
                }
              }}></input>
          </div>
          <div>
            <label>Échelle de prix</label>
            <div id="EVE-price-display">
              <p>0</p>
              <p>{priceFilter}</p>
              <p>{maxPrice}</p>
            </div>
            <input
              onChange={(e) => {
                setPriceFilter(e.target.value);
                if (e.target.value !== 0) {
                  document.getElementById("EVE-free").checked = false;
                }
              }}
              id="EVE-range"
              min="0"
              max={maxPrice}
              step="1"
              value={priceFilter}
              type="range"></input>
          </div>
        </div>
      */}
        <News
          number={100}
          priceFilter={priceFilter}
          setPriceFilter={setPriceFilter}
          setMaxPrice={setMaxPrice}
        />
      </div>
      <Footer />
    </div>
  );
}
