/** @format */

import React from "react";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import contactImg from "../../assets/img/contactImg.png";

import "../../assets/styles/Contact.scss";

export default function Contact() {
  return (
    <div>
      <Menu />
      <div className="Contact-background">
        <h1>Contactez nous !</h1>
        <div className="Contact-1">
          <img
            className="Contact-IMG"
            src={contactImg}
            alt="illustration de conctact"
          />
        </div>
        <a href="mailto:contact@standup-lyon.fr" className="Contact-link">
          contact@standup-lyon.fr
        </a>
      </div>
      <Footer />
    </div>
  );
}
