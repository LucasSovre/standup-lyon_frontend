/** @format */

import React from "react";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import ModalFAQ from "../../components/ModalFAQ";

import "../../assets/styles/FAQ.scss";
import data from "../../assets/dataJson/FAQ.json";

export default function FAQ() {
  return (
    <div>
      <Menu />
      <div className="FAQ-main">
        <h2>F.A.Q</h2>
        <div className="FAQ-list">
          {data.map((question) => {
            return <ModalFAQ key={question.id} content={question} />;
          })}
        </div>
      </div>
      <Footer />
    </div>
  );
}
