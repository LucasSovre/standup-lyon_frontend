/** @format */

import React, { useState } from "react";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import { base_url } from "../../utils/backend";

import "../../assets/styles/newsletterUnsubscribe.css";

import { MdOutlineUnsubscribe } from "react-icons/md";

export default function NewsletterUnsubscribe() {
  const [email, setEmail] = useState("");

  async function unsubscribe() {
    const settings = {
      email: email,
    };
    fetch(base_url + "/newsletter/", {
      body: new URLSearchParams(settings),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "DELETE",
    }).then((response) => {
      if (response.status === 200) {
        document.getElementById("NST-U-icon").style.color = "white";
        document.getElementById("NST-U-icon").style.backgroundColor = "green";
      }
    });
  }

  return (
    <div>
      <Menu />
      <div>
        <h3 id="NST-U-title">Se désinscrire de la newsletter :</h3>
        <div className="NST-U_form">
          <div id="NST-U-icon">
            <MdOutlineUnsubscribe />
          </div>
          <div className="NST-U_item">
            <input
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              type="email"
            />
            <button
              onClick={() => {
                unsubscribe();
              }}>
              se désinscrire
            </button>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
