/** @format */
// eslint-disable-next-line
import React, { useState } from "react";
import Menu from "../../components/Menu";
import axios from "axios";
import Cookies from "universal-cookie";
import "../../assets/styles/CreateEvent.scss";
import "../../assets/styles/Forms.scss";
import { compressImg } from "../../utils/compressImg";
import { useSelector } from "react-redux";

import { base_url } from "../../utils/backend";
import progress from "../../assets/img/eventCreation.svg";
import Name from "../../components/createEvent/name";
import Book from "../../components/createEvent/book";
import Recurrency from "../../components/createEvent/recurrency";
import Place from "../../components/createEvent/place";
import Recap from "../../components/createEvent/recap";
import Confirm from "../../components/createEvent/confirm";

export default function CreateEvent() {
  const cookies = new Cookies();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [book, setBook] = useState(false);
  const [bookLink, setBookLink] = useState("");
  const [price, setPrice] = useState(0);
  const [poster, setPoster] = useState(undefined);
  const [is_recurrent, setRecurrrency] = useState(false);
  const [adresse, setAdresse] = useState("");
  const [artists, setArtists] = useState({});
  const [dateList, setDateList] = useState({});
  const [hour, setHour] = useState("");
  const [succes, setSucces] = useState(null);
  const [warning, setWarning] = useState("");
  const profile = useSelector((state) => state.profil.user);

  function calculSold() {
    if (price === 0) {
      return "free";
    }
    if (price < 0) {
      return "hat";
    } else return price;
  }

  function DateListWithHour() {
    const tmp = {};
    for (const [key, value] of Object.entries(dateList)) {
      tmp[key] = value + "T" + hour;
    }
    return tmp;
  }

  function postEvent() {
    const FormData = require("form-data");
    if (poster !== undefined) {
      compressImg(poster).then((compressedPoster) => {
        let data = new FormData();
        data.append("adresse", adresse);
        data.append("artists", JSON.stringify(artists));
        data.append("book", book);
        data.append("bookLink", bookLink);
        data.append("creator", profile.username);
        data.append(
          "cover",
          compressedPoster
            ? new File([compressedPoster], compressedPoster.name)
            : null
        );
        data.append("description", description);
        data.append("name", title);
        data.append(
          "posterExist",
          poster === null || poster === undefined ? false : true
        );
        data.append("price", price);
        data.append("sold", calculSold());
        data.append("dates_list", JSON.stringify(DateListWithHour()));

        let config = {
          method: "post",
          url: base_url + "/event/",
          headers: {
            Authorization: "Bearer " + cookies.get("token"),
          },
          data: data,
        };

        axios
          .request(config)
          .then((response) => {
            setSucces(true);
          })
          .catch((error) => {
            const messageObject = JSON.parse(error.request.response);
            let message = "";
            for (const [key, value] of Object.entries(messageObject)) {
              message += key + "=" + value + " ; ";
            }
            setSucces(false);
            setWarning(message);
          });
      });
    } else {
      let data = new FormData();
      data.append("adresse", adresse);
      data.append("artists", artists);
      data.append("book", book);
      data.append("bookLink", bookLink);
      data.append("creator", profile.username);
      data.append("description", description);
      data.append("name", title);
      data.append(
        "posterExist",
        poster === null || poster === undefined ? false : true
      );
      data.append("price", price);
      data.append("sold", calculSold());
      data.append("dates_list", JSON.stringify(DateListWithHour()));

      let config = {
        method: "post",
        url: base_url + "/event/",
        headers: {
          Authorization: "Bearer " + cookies.get("token"),
        },
        data: data,
      };

      axios
        .request(config)
        .then((response) => {
          if (response.data) {
            setSucces(true);
          }
        })
        .catch((error) => {
          const messageObject = JSON.parse(error.request.response);
          let message = "";
          for (const [key, value] of Object.entries(messageObject)) {
            message += key + "=" + value + " ; ";
          }
          setSucces(false);
          setWarning(message);
        });
    }
  }

  return (
    <div>
      <Menu />
      <h1 id="CE-title">
        Créer un <br />
        évènement
      </h1>
      <div id="CE-progress-bar">
        <div></div>
        <div id="CE-progress"></div>
        <img src={progress} alt="progress bar" />
      </div>

      <div id="CE-card-container">
        <Name
          title={title}
          setTitle={setTitle}
          description={description}
          setDescription={setDescription}
        />
        <Book
          book={book}
          setBook={setBook}
          price={price}
          setPrice={setPrice}
          poster={poster}
          setPoster={setPoster}
          setBookLink={setBookLink}
        />
        <Recurrency
          is_recurrent={is_recurrent}
          setRecurrrency={setRecurrrency}
          setDateList={setDateList}
        />
        <Place
          adresse={adresse}
          setAdresse={setAdresse}
          hour={hour}
          setHour={setHour}
          setMainArtists={setArtists}
        />
        <Recap
          title={title}
          adresse={adresse}
          hour={hour}
          price={price}
          description={description}
          send={postEvent}
        />
        <Confirm succes={succes} warning={warning} />
      </div>
    </div>
  );
}
