/** @format */

import React, { useEffect, useState } from "react";
import Footer from "../../components/Footer";
import Menu from "../../components/Menu";
import "../../assets/styles/CreateEvent.scss";
import "../../assets/styles/modifyEvent.scss";
import "../../assets/styles/Forms.scss";
import axios from "axios";

import { FaExchangeAlt } from "react-icons/fa";
import { MdPersonAddAlt1 } from "react-icons/md";
import {
  fetchAllArtists,
  fetchEventCover,
  fetchUniqueEvent,
} from "../../utils/API/callToApi";
import { useHistory, useParams } from "react-router-dom";
import { formatSearch, RenderSnipet } from "../../utils/adresseAutoComplete";
import { base_url } from "../../utils/backend";
import Cookies from "universal-cookie";
import { compressImg } from "../../utils/compressImg";

export default function ModifyEventPage() {
  const [id, setId] = useState("");
  const [filename, setFileName] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [affiche, setAffiche] = useState(null);
  const [artists, setArtists] = useState([]);
  const [adresse, setAdresse] = useState("");
  const [book, setBook] = useState(false);
  const [bookLink, setBookLink] = useState(false);
  const [price, setPrice] = useState(0);
  const [searchPlace, setSearchPlace] = useState([]);
  const [searchPlaceResult, setSearchPlaceResult] = useState([]);
  const [tmpArtist, setTmpArtist] = useState("");
  const [allArtists, setAllArtists] = useState([]);
  const [artistSnipest, setArtistSnipest] = useState([]);
  const cookies = new Cookies();
  const history = useHistory();

  const params = useParams();

  useEffect(() => {
    fetchAllArtists().then((response) => {
      let tmp = [];
      response.forEach((element) => {
        tmp.push(element.username);
      });
      setAllArtists(tmp);
    });
  }, []);

  useEffect(() => {
    let tmp = [];
    if (tmpArtist.length >= 1) {
      allArtists.forEach((element) => {
        if (element.toLowerCase().indexOf(tmpArtist.toLowerCase()) !== -1) {
          tmp.push(element);
        }
      });
      setArtistSnipest(tmp);
    } else {
      setArtistSnipest([]);
    }
  }, [tmpArtist, allArtists]);

  useEffect(() => {
    fetchUniqueEvent(params.eventID).then((response) => {
      setName(response.name);
      setDescription(response.description);
      setAdresse(response.adresse);
      setId(response.id);
      if (response.cover !== "") {
        fetchEventCover(response.cover).then((result) => {
          setFileName(response.cover.match(/(.jpe?g|.png)/g).slice(-1)[0]);
          result.blob().then((file) => setAffiche(file));
        });
      }
      response.bookLink !== "null"
        ? setBookLink(response.bookLink)
        : setBookLink("");
      if (response.book === "true") {
        setBook(true);
      } else {
        setBookLink("");
      }
      setArtists(Object.values(response.artists));
      price === -1
        ? document.getElementById("MOE-hat").click()
        : setPrice(response.price);
    });
  }, [params.eventID, price]);

  useEffect(() => {
    if (affiche !== null) {
      if (affiche.type !== "image/jpeg") {
        window.alert("The file have to be an image.");
      }
      document.getElementById("MOE-custome-file").style.backgroundImage =
        "url(" + URL.createObjectURL(affiche) + ")";
    }
  }, [affiche]);

  useEffect(() => {
    price === -1
      ? (document.getElementById("MOE-hat").style.backgroundColor = "#FDC961")
      : (document.getElementById("MOE-hat").style.backgroundColor =
          "rgba(173, 173, 173, 0.5)");
  }, [price]);

  useEffect(() => {
    if (searchPlace !== "https://api-adresse.data.gouv.fr/search/?q=") {
      document.getElementById("adress-input").style.borderBottomLeftRadius =
        "0px";
      document.getElementById("adress-input").style.borderBottomRightRadius =
        "0px";
      axios
        .get(searchPlace)
        .then((response) => {
          setSearchPlaceResult(response.data.features);
        })
        .catch((error) => {});
    } else {
      setSearchPlaceResult([]);
      document.getElementById("adress-input").style.borderBottomLeftRadius =
        "10px";
      document.getElementById("adress-input").style.borderBottomRightRadius =
        "10px";
    }
  }, [searchPlace]);

  function updateEvent() {
    compressImg(affiche).then((compressedPoster) => {
      const form = new FormData();
      form.append("id", id);
      form.append("name", name);
      form.append("book", book);
      form.append("bookLink", bookLink);
      form.append("description", description);
      form.append("price", price);
      form.append("name", name);
      form.append("cover", new File([compressedPoster], name + filename));
      form.append("artists", JSON.stringify(Object.assign({}, artists)));
      console.log(compressedPoster.name);
      const options = {
        method: "PATCH",
        url: base_url + "/event/",
        headers: {
          Authorization: "Bearer " + cookies.get("token"),
        },
        data: form,
      };
      axios
        .request(options)
        .then(function (response) {
          history.push("/logedIn/Dashboard");
        })
        .catch(function (error) {
          console.error(error);
        })
        .then(() => history.push("Dashboard"));
    });
  }

  return (
    <div>
      <Menu />
      <div className="MOE-main">
        <h1>Modifier évènement</h1>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            updateEvent();
          }}>
          <div>
            <label>Titre</label>
            <input
              type="text"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
          </div>
          <div>
            <label>Description</label>
            <textarea
              value={description}
              onChange={(e) => {
                setDescription(e.target.value);
              }}
            />
          </div>
          <div>
            <label>Affiche</label>
            <div
              id="MOE-custome-file"
              className="MOE-custome-file"
              onClick={() => {
                document.getElementById("file-input").click();
              }}>
              <FaExchangeAlt />
              <input
                type="file"
                id="file-input"
                accept=".jpg, .jpeg, .png"
                onChange={(e) => {
                  setAffiche(e.target.files[0]);
                  console.log(
                    e.target.files[0].name.match(/(.jpe?g|.png)/g).slice(-1)[0]
                  );
                }}
              />
            </div>
          </div>
          <div>
            <label>Artistes</label>
            <div className="MOE-artistInput-container">
              <input
                id="artistInput"
                type="text"
                autoComplete="off"
                onChange={(e) => {
                  setTmpArtist(e.target.value);
                }}
              />
              <button
                onClick={(e) => {
                  e.preventDefault();
                  setArtists([
                    ...artists,
                    document.getElementById("artistInput").value,
                  ]);
                  document.getElementById("artistInput").value = "";
                }}>
                <MdPersonAddAlt1 />
              </button>
            </div>
            <div>
              <ul className="Form-snipetList">
                {artistSnipest.map((artist) => {
                  return (
                    <li
                      id={artist}
                      key={artist}
                      onClick={(e) => {
                        setArtists([...artists, e.target.id]);
                        document.getElementById("artistInput").value = "";
                        setArtistSnipest([]);
                      }}>
                      {artist}
                    </li>
                  );
                })}
              </ul>
            </div>
            <div className="MOE-artistAdded-container">
              {artists.map((artist, i) => {
                return (
                  <div key={artist} className="MOE-artistAdded-item">
                    <p>{artist}</p>
                    <button
                      onClick={(e) => {
                        e.preventDefault();
                        setArtists([
                          ...artists.slice(0, i),
                          ...artists.slice(i + 1),
                        ]);
                      }}>
                      -
                    </button>
                  </div>
                );
              })}
            </div>
          </div>
          <div>
            <label>Adresse</label>
            <input
              type="text"
              value={adresse}
              autoComplete="off"
              id="adress-input"
              onChange={(e) => {
                formatSearch(e.target.value, setSearchPlace);
                setAdresse(e.target.value);
              }}
            />
            <RenderSnipet
              searchPlaceResult={searchPlaceResult}
              setSearchPlaceResult={setSearchPlaceResult}
              setAdresse={setAdresse}
            />
          </div>
          <div>
            <label>Reservation</label>
            <div className="MOE-radio-container">
              <div>
                <p>oui</p>
                <input
                  type="radio"
                  checked={book}
                  onChange={() => {
                    setBook(true);
                  }}
                />
              </div>
              <div>
                <p>non</p>
                <input
                  type="radio"
                  checked={!book}
                  onChange={() => {
                    setBook(false);
                  }}
                />
              </div>
            </div>
          </div>
          {book === true ? (
            <div>
              <label>Lien de la billetterie</label>
              <input
                type="text"
                value={bookLink}
                onChange={(e) => {
                  setBookLink(e.target.value);
                }}
              />
            </div>
          ) : null}
          <div>
            <label>Prix</label>
            <div className="MOE-price">
              <input
                type="number"
                value={price}
                onChange={(e) => {
                  setPrice(e.target.value);
                }}
              />
              <button
                onClick={(e) => {
                  e.preventDefault();
                  setPrice(-1);
                }}
                id="MOE-hat">
                Chapeau
              </button>
            </div>
          </div>
          <input type="submit" />
        </form>
      </div>
      <Footer />
    </div>
  );
}
