/** @format */

import React, { useState } from "react";
import Footer from "../../components/Footer";
import Menu from "../../components/Menu.jsx";
import { v4 as uuidv4 } from "uuid";

import "../../assets/styles/DeleteAccount.css";

export default function DeleteAccount() {
  const [confirmationCode] = useState(uuidv4().toString().slice(0, 8));
  const [confirmationInput, setConfirmationInput] = useState("");

  function renderDeleteButton() {
    if (confirmationInput === confirmationCode) {
      return (
        <button
          onClick={() => {
            deleteUser();
          }}>
          Supprimer mon compte
        </button>
      );
    } else {
      return <button disabled>Supprimer mon compte</button>;
    }
  }

  function deleteUser() {}

  return (
    <div>
      <Menu />
      <div className="DA-mainContainer">
        <h1>Suppimer Mon compte</h1>
        <h3>
          Si vous supprimez votre compte, toutes vos données, evenement passés
          et futurs seront éffacés définitvement.
          <br /> Comme le stipules nos mentions légales nous ne conserverons
          aucune information vous concernant.{" "}
        </h3>
        <div className="DA-confirmationContainer">
          <h4>Code de confirmation :</h4>
          <p>{confirmationCode}</p>
        </div>
        <p>
          Pour pouvoir vous désinscrire veuillez saisir le code de confirmation
          ci-dessous
        </p>
        <div className="L-formItem" style={{ marginTop: "15px" }}>
          <label>Champ de confirmation :</label>
          <input
            style={{ textAlign: "center" }}
            type="texte"
            onChange={(e) => {
              setConfirmationInput(e.target.value);
            }}
          />
        </div>
        {renderDeleteButton()}
      </div>
      <Footer />
    </div>
  );
}
