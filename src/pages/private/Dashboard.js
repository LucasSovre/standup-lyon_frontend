/** @format */

import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "../../assets/styles/Dashboard.scss";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import { useSelector } from "react-redux";
import axios from "axios";
import Cookies from "universal-cookie";
import { base_url } from "../../utils/backend";
import { fetchMyEvents } from "../../utils/API/callToApi";
import "../../assets/styles/squareEvent.scss";
import SquareEvent from "../../components/squareEvent";
import { AiOutlinePlusCircle } from "react-icons/ai";
import { MdManageAccounts, MdSecurity, MdHelp } from "react-icons/md";

export default function Dashboard() {
  const profile = useSelector((state) => state.profil);
  const [eventList, setEventList] = useState([]);

  useEffect(() => {
    if (profile.user !== undefined) {
      const cookies = new Cookies();
      const options = {
        method: "POST",
        headers: { Authorization: "Bearer " + cookies.get("token") },
      };
      if (profile.user.username) {
        axios
          .request(base_url + "/usersUpdateIp/", options)
          .then(function (response) {})
          .catch(function (error) {
            console.error(error);
          });
      }
    }
  }, [profile.user]);

  useEffect(() => {
    if (profile.user !== undefined) {
      if (profile.user.username !== undefined) {
        fetchMyEvents(profile.user.username).then((response) => {
          setEventList(response);
        });
      }
    }
  }, [profile.user]);

  return (
    <div>
      <Menu />
      <h1 className="DA-title">Dashboard</h1>
      <div className="DA-main">
        <h2>Mes evenements</h2>
        <div className="SQEV-container">
          {eventList !== []
            ? eventList.map((event) => {
                return <SquareEvent key={event.name} event={event} />;
              })
            : null}
          <Link
            className="SQEV-btn-new"
            title="Creer un nouvel evenement"
            to="/logedIn/CreateEvent">
            <AiOutlinePlusCircle />
          </Link>
        </div>
        <h2>Mon compte</h2>
        <div className="DA-account-container">
          <Link to="/logedIn/settings">
            <MdManageAccounts />
          </Link>
          <Link to="/logedIn/security">
            <MdSecurity />
          </Link>
          <Link to="/FAQ">
            <MdHelp />
          </Link>
        </div>
      </div>
      <Footer />
    </div>
  );
}
