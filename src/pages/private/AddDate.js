/** @format */

import React, { useEffect, useState } from "react";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import { useHistory, useParams } from "react-router-dom";
import DatePicker from "react-multi-date-picker";
import { BsFillCalendarPlusFill } from "react-icons/bs";
import axios from "axios";
import "../../assets/styles/AddDate.scss";
import { fetchUniqueEvent } from "../../utils/API/callToApi";
import { base_url } from "../../utils/backend";
import Cookies from "universal-cookie";

export default function AddDate() {
  const [event, setEvent] = useState(null);
  const [date, setDate] = useState([]);
  const cookies = new Cookies();
  const history = useHistory();
  const params = useParams();

  useEffect(() => {
    fetchUniqueEvent(params.eventID).then((response) => {
      setEvent(response);
    });
  }, [params.eventID]);

  useEffect(() => {
    if (event !== null) {
      let tmp = [];
      // eslint-disable-next-line
      for (const [key, value] of Object.entries(event.dates_list)) {
        tmp.push(new Date(value));
      }
      setDate(tmp);
    }
  }, [event]);

  function CustomInput({ openCalendar, value, handleValueChange }) {
    return (
      <button
        id="addDate-input"
        onFocus={openCalendar}
        value={date}
        onChange={handleValueChange}>
        <BsFillCalendarPlusFill />
      </button>
    );
  }

  function updateDates() {
    const form = new FormData();
    form.append("id", event.id);
    form.append("dates_list", JSON.stringify(Object.assign({}, date)));
    const options = {
      method: "PATCH",
      url: base_url + "/event/",
      headers: {
        Authorization: "Bearer " + cookies.get("token"),
      },
      data: form,
    };
    axios
      .request(options)
      .then(function (response) {
        history.push("/logedIn/Dashboard");
      })
      .catch(function (error) {
        console.error(error);
      })
      .then(() => history.push("Dashboard"));
  }

  return (
    <div>
      <Menu />
      <div className="addDate-main">
        <h1>Modifier les dates</h1>
        <h2>{event !== null ? event.name : null}</h2>
        <div className="addDate-list">
          {date.map((item) => {
            return (
              <div key={item} className="addDate-item">
                <div className="addDate-content">
                  <p>
                    {new Date(item)
                      .toLocaleDateString("fr-FR", {
                        day: "numeric",
                      })
                      .replace(" ", "\n")}
                  </p>
                  <p>
                    {new Date(item)
                      .toLocaleDateString("fr-FR", {
                        month: "long",
                      })
                      .replace(" ", "\n")}
                  </p>
                </div>
              </div>
            );
          })}
          <DatePicker
            multiple
            value={date}
            onChange={setDate}
            render={<CustomInput />}
          />
        </div>
        <button className="addDate-submit" onClick={() => updateDates()}>
          Confirmer
        </button>
      </div>
      <Footer />
    </div>
  );
}
