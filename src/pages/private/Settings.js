/** @format */

import React, { useEffect, useState } from "react";
import axios from "axios";
import Footer from "../../components/Footer";
import Menu from "../../components/Menu";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { MdOutlineEdit } from "react-icons/md";
import { BsFillCheckCircleFill } from "react-icons/bs";
import { formatSearch, RenderSnipet } from "../../utils/adresseAutoComplete";
import { compressImg } from "../../utils/compressImg";
import defaultAvatar from "../../assets/img/defaultAvatar.png";
import { base_url } from "../../utils/backend";
import Cookies from "universal-cookie";
import { useHistory } from "react-router-dom";
import { setProfile } from "../../actions/actionIndex";

import "../../assets/styles/Settings.scss";
import "../../assets/styles/Forms.scss";

export default function Settings() {
  const history = useHistory();
  const dispatch = useDispatch();
  const cookies = new Cookies();
  const profile = useSelector((state) => state.profil.user);
  const PP = useSelector((state) => state.currentPP.pp);
  const [tmpPP, setTmpPP] = useState(undefined);
  const [displayPP, setDisplayPP] = useState(undefined);
  const [searchPlaceResult, setSearchPlaceResult] = useState([]);
  const [searchPlace, setSearchPlace] = useState([]);

  const [description, setDescription] = useState("");
  const [instagramUserName, setInstagramUserName] = useState("");
  const [youtubeAcount, setYouTubeAcount] = useState("");
  const [twitterUserName, settwitterUserName] = useState("");
  const [first_name, setFirst_name] = useState("");
  const [last_name, setLast_name] = useState("");

  const [username, setUsername] = useState("");

  const [adresse, setAdresse] = useState("");

  const [phone, setPhone] = useState("");

  const [is_artist, setIs_artist] = useState(false);

  useEffect(() => {
    setDisplayPP(PP);
  }, [PP]);

  useEffect(() => {
    if (profile.publicData) {
      setDescription(profile.publicData.description);
      setInstagramUserName(profile.publicData.instagramUserName);
      settwitterUserName(profile.publicData.twitterUserName);
      setYouTubeAcount(profile.publicData.youtubeAcount);
    }
    if (profile.publicData) {
      setAdresse(profile.privateData.adresse);
      setPhone(profile.privateData.phone);
    }
    if (profile) {
      setFirst_name(profile.first_name);
      setLast_name(profile.last_name);
      setUsername(profile.username);
      setIs_artist(profile.is_artist);
    }
  }, [profile]);

  async function changePP() {
    if (tmpPP) {
      compressImg(tmpPP).then((pp) => {
        if (displayPP !== defaultAvatar) {
          const form = new FormData();
          form.append("username", profile.username);
          form.append(
            "image",
            new File(
              [pp],
              profile.username + tmpPP.name.slice(tmpPP.name.lastIndexOf("."))
            )
          );
          form.append("imageExist", "True");
          const options = {
            method: PP === defaultAvatar ? "POST" : "PATCH",
            url: base_url + "/photoProfil/",
            headers: {
              "Content-Type":
                "multipart/form-data; boundary=---011000010111000001101001",
              Authorization: "Bearer " + cookies.get("token"),
            },
            data: form,
          };

          axios
            .request(options)
            .then(function (response) {})
            .catch(function (error) {
              console.error(error);
            });
        }
      });
    }
  }

  async function updateUser() {
    await changePP().then(() => {
      const settings = {
        username: username,
        first_name: first_name,
        last_name: last_name,
        email: profile.email,
        is_artist: is_artist,
        publicData: JSON.stringify({
          description: description,
          instagramUserName: instagramUserName,
          youtubeAcount: youtubeAcount,
          twitterUserName: twitterUserName,
        }),
        privateData: JSON.stringify({
          adresse: adresse,
          phone: phone,
        }),
      };
      fetch(base_url + "/usersPrivateInfUpdate/", {
        body: new URLSearchParams(settings),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + cookies.get("token"),
        },
        method: "PATCH",
      })
        .then(() => dispatch(setProfile()))
        .then(() => history.push("Dashboard"));
    });
  }

  async function updateUserWithoutPush() {
    await changePP().then(() => {
      const settings = {
        username: username,
        first_name: first_name,
        last_name: last_name,
        email: profile.email,
        is_artist: is_artist,
        publicData: JSON.stringify({
          description: description,
          instagramUserName: instagramUserName,
          youtubeAcount: youtubeAcount,
          twitterUserName: twitterUserName,
        }),
        privateData: JSON.stringify({
          adresse: adresse,
          phone: phone,
        }),
      };
      fetch(base_url + "/usersPrivateInfUpdate/", {
        body: new URLSearchParams(settings),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + cookies.get("token"),
        },
        method: "PATCH",
      }).then(() => dispatch(setProfile()));
    });
  }

  useEffect(() => {
    if (searchPlace !== "https://api-adresse.data.gouv.fr/search/?q=") {
      document.getElementById("adress-input").style.borderBottomLeftRadius =
        "0px";
      document.getElementById("adress-input").style.borderBottomRightRadius =
        "0px";
      axios
        .get(searchPlace)
        .then((response) => {
          setSearchPlaceResult(response.data.features);
        })
        .catch((error) => {});
    } else {
      setSearchPlaceResult([]);
      document.getElementById("adress-input").style.borderBottomLeftRadius =
        "10px";
      document.getElementById("adress-input").style.borderBottomRightRadius =
        "10px";
    }
  }, [searchPlace]);

  return (
    <div>
      <Menu />
      <h1 id="SE-title">
        Mon <br />
        compte
      </h1>
      <div id="SE-PP">
        {displayPP === undefined ? null : (
          <img src={displayPP} alt="profile pic" />
        )}
        <div id="SE-PP-edit">
          <input
            type="file"
            accept="image/png, image/jpeg"
            onChange={(e) => {
              setDisplayPP(URL.createObjectURL(e.target.files[0]));
              setTmpPP(e.target.files[0]);
            }}
          />
          <MdOutlineEdit />
        </div>
        {profile.is_veried ? (
          <div id="SE-PP-verified">
            <BsFillCheckCircleFill />
          </div>
        ) : null}
      </div>
      <form
        className="Forms-main SE-form"
        onSubmit={(e) => {
          e.preventDefault();
          updateUser();
        }}>
        <div className="Forms-group">
          <label className="SE-input-title">Présentation :</label>
          <textarea
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
            className="SE-textArea SE-input"></textarea>
        </div>
        <div>
          <h4 className="SE-input-title">Réseaux Sociaux :</h4>
          <div className="Forms-group">
            <label className="SE-input-litlle-title">Instagram :</label>
            <input
              value={instagramUserName}
              onChange={(e) => {
                setInstagramUserName(e.target.value);
              }}
              type="text"
              className="SE-input"
            />
          </div>
          <div className="Forms-group">
            <label className="SE-input-litlle-title">Twitter :</label>
            <input
              type="text"
              value={twitterUserName}
              onChange={(e) => {
                settwitterUserName(e.target.value);
              }}
              className="SE-input"
            />
          </div>
          <div className="Forms-group">
            <label className="SE-input-litlle-title">Youtube :</label>
            <input
              type="text"
              value={youtubeAcount}
              onChange={(e) => {
                setYouTubeAcount(e.target.value);
              }}
              className="SE-input"
            />
          </div>
        </div>
        <div>
          <h4 className="SE-input-title">Informations :</h4>
          <div className="Forms-group">
            <label className="SE-input-litlle-title">
              Êtes-vous artiste de Standup ?
            </label>
            <div className="Forms-radio-container">
              <div className="Forms-radio">
                <input
                  type="radio"
                  checked={is_artist}
                  onChange={() => {
                    setIs_artist(true);
                  }}
                />
                <label>Oui</label>
              </div>
              <div className="Forms-radio">
                <input
                  type="radio"
                  checked={!is_artist}
                  onChange={() => {
                    setIs_artist(false);
                  }}
                />
                <label>Non</label>
              </div>
            </div>
          </div>
          <div className="Forms-group-double">
            <div className="Forms-group">
              <label className="SE-input-litlle-title">Prénom :</label>
              <input
                type="text"
                value={last_name}
                onChange={(e) => {
                  setLast_name(e.target.value);
                }}
                className="SE-input"
              />
            </div>
            <div className="Forms-group">
              <label className="SE-input-litlle-title">Nom :</label>
              <input
                type="text"
                value={first_name}
                onChange={(e) => {
                  setFirst_name(e.target.value);
                }}
                className="SE-input"
              />
            </div>
          </div>
          <div className="Forms-group">
            <label className="SE-input-litlle-title">Pseudo :</label>
            <input
              type="text"
              value={username}
              onChange={(e) => {
                setUsername(e.target.value);
              }}
              className="SE-input"
            />
          </div>
          <div className="Forms-group">
            <label className="SE-input-litlle-title">Adresse :</label>
            <input
              type="text"
              className="SE-input"
              autoComplete="off"
              id="adress-input"
              value={adresse}
              onChange={(e) => {
                formatSearch(e.target.value, setSearchPlace);
                setAdresse(e.target.value);
              }}
            />
            <RenderSnipet
              searchPlaceResult={searchPlaceResult}
              setSearchPlaceResult={setSearchPlaceResult}
              setAdresse={setAdresse}
            />
          </div>
          <div className="Forms-group">
            <label className="SE-input-litlle-title">Téléphone :</label>
            <input
              type="text"
              value={phone}
              onChange={(e) => {
                setPhone(e.target.value);
              }}
              className="SE-input"
            />
          </div>
        </div>
        {profile.is_verified === true ? null : (
          <Link
            className="SE-link"
            onClick={() => {
              updateUserWithoutPush();
            }}
            to="/logedIn/askVerified">
            Devenir verifié
          </Link>
        )}
        <input type="submit" className="SE-input-button" />
      </form>
      <Footer />
    </div>
  );
}
