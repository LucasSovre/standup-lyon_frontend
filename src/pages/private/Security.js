/** @format */
import Footer from "../../components/Footer";
import Menu from "../../components/Menu.jsx";

import "../../assets/styles/Security.scss";

export default function Security() {
  return (
    <div>
      <Menu />
      <div className="F-container">
        <p>
          Cette section est actuellement en developpement. Pour exercer vos
          droits en matière de RGPD, à savoir :
        </p>
        <ul>
          <li>Rectifier vos informations personelles.</li>
          <li>Supprimez vos données.</li>
          <li>Connaitre les données récoltées.</li>
          <li>S'opposer à la récolte de vos données</li>
        </ul>
        <p>Veuillez nous contacter à admin@standup-lyon.fr</p>
      </div>
      <Footer />
    </div>
  );
}
