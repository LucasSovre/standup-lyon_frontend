/** @format */

import React, { useEffect } from "react";
import Menu from "../../components/Menu";
import { MdEmail } from "react-icons/md";
import { BsFillPersonLinesFill, BsFillTelephoneFill } from "react-icons/bs";
import { useSelector } from "react-redux";

import "../../assets/styles/askVerified.scss";

export default function AskVerified() {
  const profile = useSelector((state) => state.profil.user);

  useEffect(() => {
    if (profile.privateData) {
      if (profile.privateData.adresse !== "") {
        document.getElementById("info").style.color = "green";
      } else {
        document.getElementById("info").style.color = "red";
      }
      if (profile.privateData.phone !== "") {
        document.getElementById("phone").style.color = "green";
      }
      if (
        profile.privateData.phone !== "" &&
        profile.privateData.adresse !== "" &&
        profile.email_verified
      ) {
        document.getElementById("AS-ask-btn").classList.remove("AS-unable");
      }
    } else {
      document.getElementById("phone").style.color = "red";
      document.getElementById("info").style.color = "red";
    }
    if (profile.email_verified) {
      document.getElementById("email").style.color = "green";
    } else {
      document.getElementById("email").style.color = "red";
    }
  }, [profile]);

  return (
    <div>
      <Menu />
      <div id="AS">
        <h1>Devenir vérifié.</h1>
        <div id="AS-body">
          <h4>
            Pour devenir vérifié et pouvoir créer vos propre événements, il faut
            : avoir vérifié son adresse mail, avoir rempli adresse et numéro de
            téléphone.
          </h4>
          <div>
            <span id="email">
              <MdEmail />
            </span>
            <span id="info">
              <BsFillPersonLinesFill />
            </span>
            <span id="phone">
              <BsFillTelephoneFill />
            </span>
          </div>
          <a
            id="AS-ask-btn"
            className="AS-unable"
            href={
              "mailto:admin@standup-lyon.fr?subject=Demande de verification&body=Je souhaite faire vérifier le compte " +
              profile.id
            }>
            Demander la verification
          </a>
        </div>
      </div>
    </div>
  );
}
