import {actions_Types} from '../actions/actionsType';
import defaultPP from "../assets/img/defaultAvatar.png";


const initialState = {pp :  defaultPP};

export default function UidReducer(state = initialState, action) {
  switch (action.type) {
    case actions_Types.pp:
      return {
        pp : action.payload,
      };
      case actions_Types.reset:
      return {
        pp : defaultPP
      };
    default:
      return state;
  }
}
