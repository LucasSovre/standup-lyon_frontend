import { createStore, applyMiddleware } from "redux";
import {profilReducer} from "../actions/actionIndex";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { combineReducers } from "redux";
import thunkMiddleware from 'redux-thunk'
import UidReducer from "./reducer_pp"
const invariant = require("redux-immutable-state-invariant").default();



const rootReducer = combineReducers({
    // here we link reducers and store's varaiable as below :
    // store's variable : reducer
    currentPP : UidReducer,
    profil : profilReducer.reducer
});

export default createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(invariant, thunk, thunkMiddleware))
  //applyMiddleware is everythings we do before each store modification
);
