import {actions_Types} from './actionsType'
import Cookies from 'universal-cookie';
import {base_url} from "../utils/backend";
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

function setPP(htmlObject) {
  return function (dispatch) {
    dispatch({ type: actions_Types.pp, payload: htmlObject });
  };
}


const setProfile = createAsyncThunk(
  actions_Types.setProfile,
  async (dispatch, getState) => {
    const cookies = new Cookies();
    return await fetch(base_url+"/usersInf/", {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + cookies.get('token')},
      method: "GET"
    }
    ).then( (res) => res.json())
  }
)

const profileSlice = createSlice({
  name: "user",
  initialState : {
    user : {},
    status : null
  },
  extraReducers: {
    [setProfile.pending]: (state, action) => {
      state.status = "loading"
    },
    [setProfile.fulfilled] : (state, action) =>{
      state.status = "succes";
      state.user = action.payload[0]
    },
    [setProfile.rejected] : (state, action) =>{
      state.status = "failed"
    }
  }
})

export {profileSlice as profilReducer, setProfile, setPP};