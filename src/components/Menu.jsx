/** @format */

import React, { useEffect } from "react";
import "../assets/styles/Menu.scss";
import { Link } from "react-router-dom";
import { ImCross } from "react-icons/im";
import { HiMenu } from "react-icons/hi";
import { setProfile } from "../actions/actionIndex";
import { useSelector, useDispatch } from "react-redux";
import { logout } from "../utils/backend";
import Cookies from "universal-cookie";

function WelcomeHeader() {
  const cookies = new Cookies();
  const dispatch = useDispatch();
  const token = cookies.get("token");
  const profile = useSelector((state) => state.profil);

  useEffect(() => {
    if (token !== undefined && !profile.user) {
      dispatch(setProfile());
    }
  }, [profile, token, dispatch]);

  return (
    <div>
      <button
        id="menu-toggle"
        onClick={() => {
          document
            .getElementById("SideMenu")
            .classList.toggle("SideMenu-active");
        }}>
        <HiMenu />
      </button>
      <div id="SideMenu">
        <div id="SideMenu-title">
          {token !== undefined && profile.user !== undefined ? (
            <h1>{profile.user.username ? profile.user.username : ""}</h1>
          ) : (
            <h1>Stand Up Lyon</h1>
          )}
          <ImCross
            onClick={() => {
              document
                .getElementById("SideMenu")
                .classList.toggle("SideMenu-active");
            }}
          />
        </div>
        <nav>
          {token !== undefined && profile.user !== undefined ? (
            <Link to="/logedIn/Dashboard">Dashboard</Link>
          ) : (
            <Link to="/">Accueil</Link>
          )}
          <Link to="/Evenements">Évenement</Link>
          <Link to="/artist">Les humoristes</Link>
          {token !== undefined && profile.user !== undefined ? (
            <Link to="/logedIn/settings">Mon compte</Link>
          ) : (
            <Link to="/login">Connexion</Link>
          )}
          <Link className="SideMenu-subLink" to="/Qui-sommes-nous">
            Qui sommes nous ?
          </Link>
          <Link className="SideMenu-subLink" to="/FAQ">
            F.A.Q
          </Link>
          <Link className="SideMenu-subLink" to="/contact">
            Contact
          </Link>
          <Link className="SideMenu-subLink" to="/mentionsLegales">
            Mentions légales
          </Link>
        </nav>
        {token !== undefined && profile.user !== undefined ? (
          <button
            id="SideMenu-help"
            onClick={() => {
              logout();
            }}>
            Déconnexion
          </button>
        ) : (
          <a
            id="SideMenu-help"
            href="https://lydia-app.com/collect/71202-standup-lyon-com/fr">
            Nous aider
          </a>
        )}
      </div>
      <div id="TopMenu">
        <Link id="logo" to="">
          Standup-lyon
        </Link>
        <div className="topLink">
          <Link to="/Evenements">Évenement</Link>
          <Link to="/artist">Les humoristes</Link>
          <Link to="/FAQ">F.A.Q</Link>
          <Link to="/contact">Contact</Link>
        </div>
        {token !== undefined && profile.user !== undefined ? (
          <Link to="/logedIn/Dashboard" className="topConnect">
            Dashboard
          </Link>
        ) : (
          <Link to="/login" className="topConnect">
            Connexion
          </Link>
        )}
      </div>
    </div>
  );
}

export default WelcomeHeader;
