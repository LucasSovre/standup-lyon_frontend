/** @format */

import React, { useEffect, useState, useCallback } from "react";
import { Link } from "react-router-dom";
import { base_url } from "../utils/backend";
import axios from "axios";

import defaultAvatar from "../assets/img/defaultAvatar.png";
import { GoVerified } from "react-icons/go";

export default function LittleArtist({ artist }) {
  const [PP, setPP] = useState(defaultAvatar);

  const fetchArtistList = useCallback(async () => {
    const options = {
      method: "GET",
      url: base_url + "/photoProfil/",
      headers: { "Content-Type": "application/json" },
      responseType: "blob",
      params: { username: artist.username },
    };
    try {
      const resp = await axios
        .get(base_url + "/photoProfil/", options)
        .then(function (response) {
          return new Blob([response.data], { type: "image/jpeg" });
        });
      setPP(URL.createObjectURL(resp));
      return resp;
    } catch (err) {}
  }, [artist.username]);

  useEffect(() => {
    fetchArtistList();
  }, [artist.username, fetchArtistList]);

  return (
    <div>
      <Link
        to={"/artist/" + artist.username}
        style={{ color: "black", textDecoration: "none" }}>
        <div className="HU-artistCarousselItem">
          <img
            className="HU-Pp"
            src={PP}
            alt={artist.username + " photo de profil"}
          />
          <h3>{artist.username}</h3>
          <div className="HU-iconVerified">
            {artist.is_verified ? <GoVerified /> : null}
          </div>
        </div>
      </Link>
    </div>
  );
}
