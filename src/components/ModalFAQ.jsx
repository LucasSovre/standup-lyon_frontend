/** @format */

import React from "react";
import "../assets/styles/ModalFAQ.scss";

export default function ModalFAQ({ content }) {
  return (
    <div
      className="Modal-FAQcontainer"
      id={content.id}
      onClick={() => {
        document.getElementById(content.id).classList.toggle("Modal-open");
      }}>
      <div className="Modal-title" onClick={() => {}}>
        <h4>{content.title}</h4>
        <span>{"<"}</span>
      </div>
      <p>{content.answer}</p>
    </div>
  );
}
