/** @format */

import React from "react";
import { MdSmsFailed } from "react-icons/md";
import { FaCalendarCheck } from "react-icons/fa";
import { Link } from "react-router-dom";

export default function Confirm({ succes, warning }) {
  return (
    <div id="CE-f" className="CE-card" style={{ visibility: "hidden" }}>
      {succes === null ? (
        <div className="lds-ring CE-loader">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      ) : null}
      {succes === true ? (
        <div className="CE-succes">
          <div>
            <FaCalendarCheck />
          </div>
          <h5>L'événement a été créé.</h5>
          <Link to="/logedIn/Dashboard">Accueil</Link>
        </div>
      ) : null}
      {succes === false ? (
        <div className="CE-fail">
          <div>
            <MdSmsFailed />
          </div>
          <h5>L'événement n'a pas été créé.</h5>
          <h6>Message d'erreur :</h6>
          <p>{warning}</p>
          <span
            onClick={() => {
              window.location.reload(false);
            }}>
            Réessayer
          </span>
        </div>
      ) : null}
    </div>
  );
}
