/** @format */

import React, { useEffect, useState } from "react";
import { BsFillArrowRightCircleFill } from "react-icons/bs";
import DatePicker from "react-multi-date-picker";

export default function Recurrency({
  is_recurrent,
  setRecurrrency,
  setDateList,
}) {
  const [tmpDate, setTmpDate] = useState([]);
  const [uniqueDate, setUniqueDate] = useState(null);

  useEffect(() => {
    if (uniqueDate) {
      setDateList([uniqueDate.toDate().toISOString().slice(0, 10)]);
    }
  }, [uniqueDate, setDateList]);

  useEffect(() => {
    const tmp = {};
    let i = 0;
    tmpDate.forEach((element) => {
      tmp[i] = element.toDate().toISOString().slice(0, 10);
      i++;
    });
    setDateList(tmp);
  }, [tmpDate, setDateList]);

  useEffect(() => {
    if (is_recurrent === true) {
      const tmp = {};
      let i = 0;
      tmpDate.forEach((element) => {
        tmp[i] = element.toDate().toISOString().slice(0, 10);
        i++;
      });
      setDateList(tmp);
    } else {
      setDateList({});
    }
  }, [is_recurrent, setDateList, tmpDate]);

  return (
    <div id="CE-c" className="CE-card" style={{ visibility: "hidden" }}>
      <div className="Forms-main CE-form">
        <div className="Forms-group CE-form">
          <label>Récurrent :</label>
          <div className="Forms-radio-container">
            <div className="Forms-radio">
              <input
                type="radio"
                checked={is_recurrent}
                onChange={() => {
                  setRecurrrency(true);
                }}
              />
              <label>Oui</label>
            </div>
            <div className="Forms-radio">
              <input
                type="radio"
                checked={!is_recurrent}
                onChange={() => {
                  setRecurrrency(false);
                }}
              />
              <label>Non</label>
            </div>
          </div>
        </div>
        {is_recurrent ? null : (
          <div className="Forms-group CE-form">
            <label>Date :</label>
            <DatePicker value={uniqueDate} onChange={setUniqueDate} />
          </div>
        )}
        {is_recurrent ? (
          <div className="Forms-group CE-form">
            <label>Dates :</label>
            <DatePicker multiple value={tmpDate} onChange={setTmpDate} />
          </div>
        ) : null}
      </div>
      <div className="CE-nav">
        <button
          className="CE-btn-previous"
          onClick={() => {
            document.getElementById("CE-b").classList.remove("CE-next");
            document.getElementById("CE-b").style.visibility = "visible";
            document.getElementById("CE-b").classList.add("CE-previous");
            document.getElementById("CE-progress").style.width = "25.4%";
            setTimeout(() => {
              document.getElementById("CE-c").style.visibility = "hidden";
            }, 500);
          }}>
          <BsFillArrowRightCircleFill />
        </button>
        <button
          className="CE-btn-next"
          onClick={() => {
            document.getElementById("CE-d").style.visibility = "visible";
            document.getElementById("CE-c").classList.remove("CE-previous");
            document.getElementById("CE-c").classList.add("CE-next");
            setTimeout(() => {
              document.getElementById("CE-c").style.visibility = "hidden";
            }, 500);
            document.getElementById("CE-progress").style.width = "62%";
          }}>
          <BsFillArrowRightCircleFill />
        </button>
      </div>
    </div>
  );
}
