/** @format */

import React, { useState, useEffect } from "react";
import { base_url } from "../../utils/backend";
import axios from "axios";
import { TiUserDelete } from "react-icons/ti";
import { BsFillPersonPlusFill } from "react-icons/bs";
import { BsFillArrowRightCircleFill } from "react-icons/bs";
import { formatSearch, RenderSnipet } from "../../utils/adresseAutoComplete";

export default function Place({
  adresse,
  setAdresse,
  hour,
  setHour,
  setMainArtists,
}) {
  const [searchPlaceResult, setSearchPlaceResult] = useState([]);
  const [searchPlace, setSearchPlace] = useState([]);
  const [artistsAdded, setArtistAdded] = useState([]);
  const [currentArtist, setCurrentArtist] = useState("");
  const [artistList, setartistList] = useState([]);
  const [artistListSearch, setartistListSearch] = useState([]);

  useEffect(() => {
    fetch(base_url + "/usersPublicInfGet/", {
      method: "GET",
      headers: {
        "Content-Type":
          "multipart/form-data; boundary=---011000010111000001101001",
      },
    }).then((response) => {
      response.json().then((data) => {
        const tempArray = [];
        for (const i in data) {
          tempArray.push(data[i].username);
        }
        setartistList(tempArray);
      });
    });
  }, []);

  function artistListSearchFunction(entry) {
    let temporaryArray = [];
    if (entry.length >= 2) {
      for (const i in artistList) {
        if (artistList[i].toLowerCase().includes(entry.toLowerCase())) {
          temporaryArray.push(artistList[i]);
        }
      }
    }
    setartistListSearch([...temporaryArray]);
  }

  useEffect(() => {
    if (searchPlace !== "https://api-adresse.data.gouv.fr/search/?q=") {
      axios
        .get(searchPlace)
        .then((response) => {
          setSearchPlaceResult(response.data.features);
        })
        .catch((error) => {});
    } else {
      setSearchPlaceResult([]);
    }
  }, [searchPlace]);

  useEffect(() => {
    function artistsToDict() {
      var result = {};
      for (const i in artistsAdded) {
        result[i] = artistsAdded[i];
      }
      return result;
    }
    setMainArtists(artistsToDict());
  }, [artistsAdded, setMainArtists]);

  return (
    <div id="CE-d" className="CE-card" style={{ visibility: "hidden" }}>
      <div className="Forms-main CE-form">
        <div className="Forms-group">
          <label>Heure :</label>
          <input
            value={hour}
            onChange={(e) => setHour(e.target.value)}
            type="time"
          />
        </div>
        <div className="Forms-group">
          <label>Adresse :</label>
          <input
            type="text"
            id="adress-input"
            onChange={(e) => {
              formatSearch(e.target.value, setSearchPlace);
              setAdresse(e.target.value);
            }}
          />
          <RenderSnipet
            searchPlaceResult={searchPlaceResult}
            setSearchPlaceResult={setSearchPlaceResult}
            setAdresse={setAdresse}
          />
        </div>
        <div className="Forms-group">
          <label className="CE-label">Artistes :</label>
          <div className="CE-artist-searchBar">
            <input
              autoComplete="off"
              className="CE-text-input"
              type="text"
              id="artiste-add"
              placeholder="Ajoutez des artistes ..."
              onChange={(e) => {
                artistListSearchFunction(e.target.value);
                setCurrentArtist(e.target.value);
              }}
            />
            <button
              className="CE-add-artist"
              onClick={(e) => {
                e.preventDefault();
                setartistListSearch([]);
                setArtistAdded([...artistsAdded, currentArtist]);
                setCurrentArtist("");
                document.getElementById("artiste-add").value = "";
              }}>
              <BsFillPersonPlusFill />
            </button>
          </div>
          <ul className="CE-list-snipet">
            {artistListSearch.map((item, i) => {
              if (i < 3) {
                return (
                  <li
                    key={item + Math.random()}
                    className="CE-snipet-search"
                    onClick={() => {
                      document.getElementById("artiste-add").value = item;
                      setartistListSearch([]);
                      setCurrentArtist(item);
                      setArtistAdded([...artistsAdded, item]);
                      document.getElementById("artiste-add").value = "";
                    }}>
                    {item}
                  </li>
                );
              } else {
                return <div></div>;
              }
            })}
          </ul>
          <div className="CE-artistContainer">
            {artistsAdded.map((item, index) => {
              return (
                <div className="CE-artist-item" key={item}>
                  <div>{item}</div>
                  <div
                    className="Ce-remove-artist"
                    onClick={() => {
                      let a = [...artistsAdded];
                      let b = [...artistsAdded];
                      setArtistAdded([
                        ...a.splice(0, index),
                        ...b.splice(index + 1, b.length),
                      ]);
                    }}>
                    <TiUserDelete />
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <div className="CE-nav">
        <button
          className="CE-btn-previous"
          onClick={() => {
            document.getElementById("CE-c").classList.remove("CE-next");
            document.getElementById("CE-c").style.visibility = "visible";
            document.getElementById("CE-c").classList.add("CE-previous");
            document.getElementById("CE-progress").style.width = "43%";
            setTimeout(() => {
              document.getElementById("CE-d").style.visibility = "hidden";
            }, 500);
          }}>
          <BsFillArrowRightCircleFill />
        </button>
        <button
          className="CE-btn-next"
          onClick={() => {
            document.getElementById("CE-e").style.visibility = "visible";
            document.getElementById("CE-d").classList.remove("CE-previous");
            document.getElementById("CE-d").classList.add("CE-next");
            setTimeout(() => {
              document.getElementById("CE-d").style.visibility = "hidden";
            }, 500);
            document.getElementById("CE-progress").style.width = "80%";
          }}>
          <BsFillArrowRightCircleFill />
        </button>
      </div>
    </div>
  );
}
