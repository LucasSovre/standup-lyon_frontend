/** @format */

import React, { useEffect, useState } from "react";
import { BsFillArrowRightCircleFill } from "react-icons/bs";

export default function Book({
  book,
  setBook,
  setPrice,
  setBookLink,
  setPoster,
}) {
  const [hat, setHat] = useState(false);

  useEffect(() => {
    if (hat) {
      document.getElementById("CE-hat").classList.add("CE-hat-active");
      document.getElementById("CE-price-in").disabled = true;
      setPrice(-1);
    } else {
      document.getElementById("CE-hat").classList.remove("CE-hat-active");
      document.getElementById("CE-price-in").disabled = false;
    }
  }, [hat, setPrice]);

  return (
    <div id="CE-b" className="CE-card" style={{ visibility: "hidden" }}>
      <div className="Forms-main CE-form">
        <label id="CE-label">Réservation :</label>
        <div className="Forms-radio-container">
          <div className="Forms-radio">
            <input
              type="radio"
              checked={book}
              onChange={() => {
                setBook(true);
              }}
            />
            <label>Oui</label>
          </div>
          <div className="Forms-radio">
            <input
              type="radio"
              checked={!book}
              onChange={() => {
                setBook(false);
              }}
            />
            <label>Non</label>
          </div>
        </div>
        {book ? (
          <div className="Forms-group CE-form">
            <label>lien de la billetterie :</label>
            <input
              type="text"
              onChange={(e) => {
                setBookLink(e.target.value);
              }}
            />
          </div>
        ) : null}
        <div className="Forms-group CE-form">
          <label>Prix :</label>
          <div id="CE-price">
            <input
              id="CE-price-in"
              type="number"
              onChange={(e) => {
                setPrice(e.target.value);
              }}
            />
            <div
              onClick={() => {
                setHat(!hat);
              }}
              id="CE-hat">
              Chapeau
            </div>
          </div>
        </div>
        <div className="Forms-group CE-form">
          <label>Affiche de l'évènement :</label>
          <input
            accept="image/png, image/jpeg"
            onChange={(e) => {
              setPoster(e.target.files[0]);
            }}
            type="file"
          />
        </div>
      </div>
      <div className="CE-nav">
        <button
          className="CE-btn-previous"
          onClick={() => {
            document.getElementById("CE-a").classList.remove("CE-next");
            document.getElementById("CE-a").style.visibility = "visible";
            document.getElementById("CE-a").classList.add("CE-previous");
            document.getElementById("CE-progress").style.width = "0%";
            setTimeout(() => {
              document.getElementById("CE-b").style.visibility = "hidden";
            }, 500);
          }}>
          <BsFillArrowRightCircleFill />
        </button>
        <button
          className="CE-btn-next"
          onClick={() => {
            document.getElementById("CE-c").style.visibility = "visible";
            document.getElementById("CE-b").classList.remove("CE-previous");
            document.getElementById("CE-b").classList.add("CE-next");
            setTimeout(() => {
              document.getElementById("CE-b").style.visibility = "hidden";
            }, 500);
            document.getElementById("CE-progress").style.width = "43%";
          }}>
          <BsFillArrowRightCircleFill />
        </button>
      </div>
    </div>
  );
}
