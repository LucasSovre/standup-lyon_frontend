/** @format */

import React from "react";
import { BsFillArrowRightCircleFill } from "react-icons/bs";

export default function Recap({
  title,
  adresse,
  hour,
  price,
  description,
  send,
}) {
  function renderPrice(number) {
    if (number < 0) {
      return "au chapeau";
    }
    if (number === 0) {
      return "gratuit";
    }
    return number + " €";
  }

  return (
    <div id="CE-e" className="CE-card" style={{ visibility: "hidden" }}>
      <div className="CE-recap">
        <h2>Récapitulatif</h2>
        <div className="CE-recap-item">
          <h5>{title}</h5>
          <p>{description}</p>
        </div>
        <div className="CE-recap-item">
          <h4>heure :</h4>
          <p>{hour}</p>
        </div>
        <div className="CE-recap-item">
          <h4>prix :</h4>
          <p>{renderPrice(price)}</p>
        </div>
        <div className="CE-recap-item">
          <h4>adresse</h4>
          <p>{adresse}</p>
        </div>
      </div>
      <div className="CE-nav">
        <button
          className="CE-btn-previous"
          onClick={() => {
            document.getElementById("CE-d").classList.remove("CE-next");
            document.getElementById("CE-d").style.visibility = "visible";
            document.getElementById("CE-d").classList.add("CE-previous");
            document.getElementById("CE-progress").style.width = "80%";
            setTimeout(() => {
              document.getElementById("CE-e").style.visibility = "hidden";
            }, 500);
          }}>
          <BsFillArrowRightCircleFill />
        </button>
        <button
          className="CE-btn-next"
          onClick={() => {
            document.getElementById("CE-f").style.visibility = "visible";
            document.getElementById("CE-e").classList.remove("CE-previous");
            document.getElementById("CE-e").classList.add("CE-next");
            setTimeout(() => {
              document.getElementById("CE-e").style.visibility = "hidden";
            }, 500);
            document.getElementById("CE-progress").style.width = "100%";
            send();
          }}>
          <BsFillArrowRightCircleFill />
        </button>
      </div>
    </div>
  );
}
