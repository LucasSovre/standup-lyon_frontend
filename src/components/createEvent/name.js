/** @format */

import React from "react";
import { BsFillArrowRightCircleFill } from "react-icons/bs";

export default function Name({ title, setTitle, description, setDescription }) {
  return (
    <div className="CE-card" id="CE-a">
      <div className="Forms-main">
        <div className="Forms-group CE-form">
          <label>Titre de l'évènement :</label>
          <input
            value={title}
            onChange={(e) => {
              setTitle(e.target.value);
            }}
            placeholder="Votre titre"
            type="text"
          />
        </div>
        <div className="Forms-group CE-form">
          <label>Decription :</label>
          <textarea
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
            placeholder="Votre description"></textarea>
        </div>
        <button
          className="CE-btn-next"
          onClick={() => {
            document.getElementById("CE-b").style.visibility = "visible";
            document.getElementById("CE-a").classList.remove("CE-previous");
            document.getElementById("CE-a").classList.add("CE-next");
            setTimeout(() => {
              document.getElementById("CE-a").style.visibility = "hidden";
            }, 500);
            document.getElementById("CE-progress").style.width = "25.4%";
          }}>
          <BsFillArrowRightCircleFill />
        </button>
      </div>
    </div>
  );
}
