/** @format */

import React, { useEffect, useState, useCallback } from "react";
import { Link } from "react-router-dom";
import { base_url } from "../utils/backend";
import axios from "axios";

import "../assets/styles/artistIcon.css";

import defaultAvatar from "../assets/img/defaultAvatar.png";

export default function ArtistIcon({ artist }) {
  const [PP, setPP] = useState(defaultAvatar);

  const fetchArtistList = useCallback(async () => {
    const options = {
      method: "GET",
      url: base_url + "/photoProfil/",
      headers: { "Content-Type": "application/json" },
      responseType: "blob",
      params: { username: String(artist) },
    };
    try {
      const resp = await axios
        .get(base_url + "/photoProfil/", options)
        .then(function (response) {
          return new Blob([response.data], { type: "image/jpeg" });
        });
      setPP(URL.createObjectURL(resp));
      return resp;
    } catch (err) {}
  }, [artist]);

  useEffect(() => {
    fetchArtistList();
  }, [artist, fetchArtistList]);

  return (
    <div>
      <Link
        to={"/artist/" + artist}
        style={{ color: "black", textDecoration: "none" }}>
        <div className="AIC-artistCarousselItem">
          <img className="AIC-Pp" src={PP} alt={artist + " photo de profil"} />
          <h3>{artist}</h3>
        </div>
      </Link>
    </div>
  );
}
