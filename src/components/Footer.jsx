/** @format */

import "../assets/styles/Footer.scss";
import { Link } from "react-router-dom";

export default function Footer() {
  return (
    <div className="Fe-main">
      <div className="Fe-column">
        <h3 className="title">Plan du site</h3>
        <ul>
          <li>
            <Link to="/">
              <button className="">Accueil</button>
            </Link>
          </li>
          <li>
            <Link to="/login">
              <button className="">Connexion</button>
            </Link>
          </li>
          <li>
            <Link to="/artist">
              <button className="">Les humoristes</button>
            </Link>
          </li>
          <li>
            <Link to="/Qui-sommes-nous">
              <button className="">Qui sommes-nous ?</button>
            </Link>
          </li>
          <li>
            <Link to="/mentionsLegales">
              <button className="">Mentions Légales</button>
            </Link>
          </li>
        </ul>
      </div>
      <div className="Fe-column">
        <h3 className="title">Infos</h3>
        <ul>
          <li>
            <p> Auteur : Lucas Sovre</p>
          </li>
          <li>
            <Link to="/contact">
              <button className="link">Contact</button>
            </Link>
          </li>
          <li>
            <Link to="/mentionsLegales">
              <button className="link">CGU</button>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}
