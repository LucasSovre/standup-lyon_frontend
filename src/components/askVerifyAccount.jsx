/** @format */

import React, { useEffect } from "react";
import { useSelector } from "react-redux";

import "../assets/styles/ModalAskVerify.css";

export default function AskVerifyAccount({ show, onUpdate }) {
  const profile = useSelector((state) => state.profil.user);

  useEffect(() => {
    if (show && profile) {
      if (profile.email_verified) {
        document.getElementById("verify-need-bar-red").style.width = "90%";
        document.getElementById("verify-need-bar-green").style.width = "10%";
        if (
          profile.privateData.city !== "" &&
          profile.privateData.adresse !== "" &&
          profile.privateData.postal_code !== ""
        ) {
          document.getElementById("verify-need-bar-red").style.width = "50%";
          document.getElementById("verify-need-bar-green").style.width = "50%";
          if (profile.privateData.phone !== "") {
            document.getElementById("verify-need-bar-red").style.width = "0%";
            document.getElementById("verify-need-bar-green").style.width =
              "100%";
          }
        }
      }
    }
  }, [profile, show]);

  function isReady() {
    if (
      profile.email_verified &&
      profile.privateData.city !== "" &&
      profile.privateData.adresse !== "" &&
      profile.privateData.postal_code !== "" &&
      profile.privateData.phone !== ""
    ) {
      return true;
    }
    return false;
  }

  function render() {
    if (show) {
      return (
        <div className="Modal-VerifyContainer">
          <div className="Modal-VerifyTitle">
            <h2>Devenir vérifié :</h2>
            <button
              onClick={() => {
                onUpdate(false);
              }}>
              X
            </button>
          </div>
          <div className="Modal-Verify-NeedContainer">
            <div id="verify-need-bar-contenair">
              <div id="verify-need-bar-green"></div>
              <div id="verify-need-bar-red"></div>
            </div>
            <div id="verify-items-container">
              <p>Email vérifé</p>
              <p>Adresse complète</p>
              <p>Numéro de téléphone</p>
            </div>
            <div className="Modal-verify-txt-container">
              <p>
                Pour faire une demande de vérification, il faut que tout les
                élèments sois complétés.
              </p>
              {isReady() ? (
                <a
                  href={
                    "mailto:admin@standup-lyon.fr?subject=Demande de verification&body=Je souhaite faire vérifier le compte " +
                    profile.id
                  }>
                  faire une demande de verification
                </a>
              ) : (
                <p>
                  Veuillez remplir toutes les informations ci dessus pour
                  pouvoir demander à être verifié.
                </p>
              )}
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
  return <div>{render()}</div>;
}
