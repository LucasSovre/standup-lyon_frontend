/** @format */

import React from "react";

export default function BetaDisclaimer() {
	return (
		<div>
			<p
				style={{
					margin: "0",
					textAlign: "center",
					backgroundColor: " #eeeeee",
				}}>
				Ce site est en bêta, son état actuel ne représente pas son rendu
				final
			</p>
		</div>
	);
}
