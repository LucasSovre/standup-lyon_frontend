/** @format */

import React from "react";
import "../assets/styles/Signalement.css";

export default function Signalement({ type, data }) {
	function render() {
		if (type === "Evénement") {
			return (
				<a
					href={
						"mailto:admin@standup-lyon.fr?subject=Signalement Event&body=Je souhaite signaler l'évenement " +
						data +
						" car il .."
					}>
					Signaler {type}
				</a>
			);
		}
		if (type === "Artiste") {
			return (
				<a
					href={
						"mailto:admin@standup-lyon.fr?subject=Signalement Artiste&body=Je souhaite signaler l'artiste " +
						data +
						" car .."
					}>
					Signaler {type}
				</a>
			);
		}
	}

	return <div className="SIG-container">{render()}</div>;
}
