import React, { useEffect } from "react";
import { RiDeleteBin7Fill } from "react-icons/ri";
import { BsFillPencilFill } from "react-icons/bs";
import { BsFillCalendarPlusFill } from "react-icons/bs";
import "../assets/styles/squareEvent.scss";
import { fetchEventCover, deleteEvent } from "../utils/API/callToApi";
import Cookies from "universal-cookie";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

export default function SquareEvent({ event }) {
  const cookies = new Cookies();
  const profile = useSelector((state) => state.profil);

  useEffect(() => {
    fetchEventCover(event.cover)
      .then((response) => response.blob())
      .then((imageBlob) => {
        document.getElementById(
          "SQEV-item" + event.name
        ).style.backgroundImage = "url(" + URL.createObjectURL(imageBlob) + ")";
      });
  }, [event.cover, event.name]);

  function deleteEventHandle() {
    if (
      window.confirm(
        "Êtes vous sure de vouloir supprimer toutes les occurences de cet évènement ?"
      )
    ) {
      deleteEvent(event.id, profile.user.username, cookies.get("token")).then(
        () => {
          window.location.reload(false);
        }
      );
    }
  }

  return (
    <div>
      <div id={"SQEV-item" + event.name} className="SQEV-item">
        <h3>{event.name}</h3>
        <div className="SQEV-bottom-container">
          <button
            title="Supprimer évènement"
            onClick={() => deleteEventHandle()}>
            <RiDeleteBin7Fill />
          </button>
          <Link title="Ajouter une date" to={"/logedIn/AddDate/" + event.id}>
            <BsFillCalendarPlusFill />
          </Link>
          <Link
            title="Modifier évènement"
            to={"/logedIn/ModifyEvent/" + event.id}>
            <BsFillPencilFill />
          </Link>
        </div>
      </div>
    </div>
  );
}
