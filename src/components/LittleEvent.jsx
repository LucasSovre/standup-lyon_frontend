import React, {useEffect, useState, useCallback} from 'react';
import {Link} from 'react-router-dom';
import axios from "axios"
import  {base_url} from "../utils/backend"

import loading from "../assets/img/loading.gif";

export default function LittleEvent ({event}) {

    const [poster, setPoster] = useState(null)

    const fetchCover = useCallback(async () => {
        const options = {
          method: 'GET',
          headers: {'Content-Type': 'application/json'},
          responseType: "blob",
          params: {file : event.cover}
        };
        axios.get(base_url+'/cover/',options).then(function (response) {
          setPoster(URL.createObjectURL(new Blob([response.data])))
        }).catch(function (error) {
          console.error(error);
        });
      },[event.cover])
  
      useEffect(() => {
        fetchCover();
      },[fetchCover])

      function displayPoster () {
        if(poster !== null){
            return(
                <div className="AP-eventItemImgContainer">
                            <img className="AP-eventItemImg" src={poster} alt='poster'></img>
                        </div>
            )
        }
        if(poster === null && event.posterExist === true){
            return(
                <div className="AP-eventItemImgContainer">
                     <img className="AP-eventItemImg" src={loading} alt='poster'></img>
                </div>
            )
        }
        else{
            return(
                <div className="AP-eventItemImgContainer">
                     <img className="AP-eventItemImg" src={event.cover} alt='poster'></img>
                </div>
            )
        }
    }

    return(
        <div className="AP-eventItem">
            <Link to={"/event/" + event.id}>
                {displayPoster()}
                <h2>{event.name}</h2>
                <h4>{event.date}</h4>
            </Link>
        </div>
    )
}

