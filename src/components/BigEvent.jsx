/** @format */

import React, { useEffect, useState, useCallback } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { base_url } from "../utils/backend";
import { date2String, renderPrice } from "../utils/eventInterpreter";
import { findNearestDate } from "../utils/eventInterpreter";

import "../assets/styles/BigEvent.scss";

export default function BigEvent({ event }) {
  const [poster, setPoster] = useState(null);
  const [date, setDate] = useState(undefined);

  const fetchCover = useCallback(async () => {
    const options = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
      responseType: "blob",
      params: { file: event.cover },
    };
    axios
      .get(base_url + "/cover/", options)
      .then(function (response) {
        setPoster(URL.createObjectURL(new Blob([response.data])));
      })
      .catch(function (error) {});
  }, [event.cover]);

  useEffect(() => {
    fetchCover();
  }, [fetchCover]);

  useEffect(() => {
    if (event !== undefined) {
      if (event.dates_list !== null) {
        const tmp = [];
        // eslint-disable-next-line
        for (const [key, value] of Object.entries(event.dates_list)) {
          tmp.push(new Date(value));
        }
        setDate(tmp);
      }
    }
  }, [event]);

  return (
    <div className="BE-item">
      <Link to={"/event/" + event.id} style={{ textDecoration: "none" }}>
        <div
          className="BE-cover"
          style={{
            backgroundImage: `url(${poster})`,
            backgroundSize: "cover",
          }}></div>
        <div className="BE-content">
          <div className="BE-detail">
            <h3>{event.name}</h3>
            <h4>
              {date !== null && date !== undefined
                ? date2String(findNearestDate(new Date(), date))
                : null}
            </h4>
            <p>{event.adresse}</p>
          </div>
          <div id="BE-price">{renderPrice(event.price)}</div>
        </div>
      </Link>
    </div>
  );
}
